
<?php get_header(); ?>

<?php

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

$cur_tag = get_query_var('tag');
$cur_tag_obj = get_term_by('slug', $cur_tag, 'post_tag');
$tag_name = strtoupper($cur_tag_obj->name);
$tag_link = get_tag_link($cur_tag_obj->term_id);
$active_class = ' class="active"';

if( 'n' == $_GET['st'] ){
  $extra_param = array( 'category__and' => array( CATE_NEWS_ID ));
} else if ( 'r' == $_GET['st'] ) {
  $extra_param = array( 'category__not_in' => array( CATE_NEWS_ID ));
} else {
  $extra_param = false;
}

$headline_list = get_latest_list_data(1, 1, $cur_tag_obj->slug, false, false, false, $extra_param);
$headline = array_pop($headline_list);
$headline_cat = $tag_name;


// sponsor tag list
$sponsor_tags = array(

  '校園作者' => array(
    'sponsor_id' => 'Campus_Main',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
     學校，一個小型的社會縮影，在這裡能看到的事物值得我們每個人思考，學習。這裡聚集了來自各校園的作者，無論你是學生、教授或者教育專家，我們都想聽見你的聲音。若你也想投稿，請來信contact@thenewslens.com。
     </div>',
    ),
);

$is_sponsor_tag = false;
$current_sponsor = '';

foreach( $sponsor_tags as $t => $a ){
  if( $t == $tag_name ){
    $is_sponsor_tag = true;
    $current_sponsor = $a;
    break;
  }
}

if($is_sponsor_tag){
  $sponsor_id = $current_sponsor['sponsor_id'];

  $page_title_height = '230';
  $page_desc = $current_sponsor['sponsor_text'];

  $bg_file = '/sponsor/tag/' . $sponsor_id . '.jpg';
  if(is_file(dirname( __FILE__ ) . $bg_file)){
    $page_title_bg = "background-image:url('" . get_template_directory_uri() . $bg_file . "'); background-size:100% auto;height:auto;";
  } else {
    $page_title_bg = '';
  }
} else {
  $page_title_height = '120';
  $page_title_bg = '';
  $page_desc = '';
}

?>
    <div class=" hero-content" style="position: absolute; color: white;  width: 100%;">
      <div style="width:400px; height:10%; background-color:#fff;"></div>
    </div>
    <div class="category-header text-center"  style="verticle-align:middle; height:<?php echo $page_title_height; ?>px; font-size:35px; padding-top:37px;<?php echo $page_title_bg; ?>">
<?php

    $page_title = wp_title( '-', false);
    $title = explode('-', $page_title); 
    echo $title[0];
    echo $page_desc;
?>
    </div>

  <div class="HolyGrail-body-post tnl-container" style="margin-top:0;">

    <main class="HolyGrail-content-post" style="margin-top:0;">
       
      <div class="category-nav" style="">
      <div class="col-lg-12">

        <div class="newest-selection">
          <ul class="nav nav-pills" style="margin-top:20px; font-weight:900;">
          <li<?php echo ($extra_param == false)?$active_class:''; ?>><a href="<?php echo $tag_link; ?>">全部文章</a></li>
          <li><a href="http://www.thenewslens.com/tag/%E4%B8%AD%E5%B1%B1%E5%A4%A7%E5%AD%B8%E4%BD%9C%E8%80%85/">中山</a></li>
          <li><a href="http://www.thenewslens.com/tag/%E4%B8%AD%E5%9C%8B%E9%86%AB%E8%97%A5%E5%A4%A7%E5%AD%B8%E4%BD%9C%E8%80%85/">中國醫</a></li>    
          <li><a href="http://www.thenewslens.com/tag/%E5%8F%B0%E7%81%A3%E5%A4%A7%E5%AD%B8%E4%BD%9C%E8%80%85/">台大</a></li>
          <li><a href="http://www.thenewslens.com/tag/%E5%8F%B0%E5%8C%97%E5%A4%A7%E5%AD%B8%E4%BD%9C%E8%80%85/">台北大</a></li>   
          <li><a href="http://www.thenewslens.com/tag/%E6%9D%B1%E5%90%B3%E5%A4%A7%E5%AD%B8%E4%BD%9C%E8%80%85/">東吳</a></li>
          <li><a href="http://www.thenewslens.com/tag/%E5%93%88%E4%BD%9B%E5%A4%A7%E5%AD%B8%E4%BD%9C%E8%80%85/">哈佛</a></li>
          <li><a href="http://www.thenewslens.com/tag/%E6%94%BF%E6%B2%BB%E5%A4%A7%E5%AD%B8%E4%BD%9C%E8%80%85/">政大</a></li>          
          <li><a href="http://www.thenewslens.com/tag/%E5%B8%AB%E7%AF%84%E5%A4%A7%E5%AD%B8%E4%BD%9C%E8%80%85/">師大</a></li>          
          <li><a href="http://www.thenewslens.com/tag/%E5%AF%86%E8%A5%BF%E6%A0%B9%E5%A4%A7%E5%AD%B8%E4%BD%9C%E8%80%85/">密西根</a></li>          
          <li><a href="http://www.thenewslens.com/tag/%E6%B8%85%E8%8F%AF%E5%A4%A7%E5%AD%B8%E4%BD%9C%E8%80%85/">清大</a></li>  
          <li><a href="http://www.thenewslens.com/tag/%E7%BE%A9%E5%AE%88%E5%A4%A7%E5%AD%B8%E4%BD%9C%E8%80%85/">義守</a></li>                        
          <li><a href="http://www.thenewslens.com/tag/%E8%BC%94%E4%BB%81%E5%A4%A7%E5%AD%B8%E4%BD%9C%E8%80%85/">輔仁</a></li>                                                  
        </ul>
      </div>

      </div>
      <div class="clearfix"></div>

      </div>

    <div class="category-nav" style="border-bottom: 0px;
    min-height: 8px !important;
    margin-top: -17px;
    margin-bottom: 10;
    color: #ADADAD; width:100%">
    <span style="float:right; margin-right:25px;">第 <?php echo $paged; ?> 頁 </span>
    </div>
<?php
$item_count = 10;


$story_list = get_latest_list_data($item_count, $paged, $cur_tag_obj->slug, false, false, false, $extra_param);

if ( count($story_list) > 0 ) {
  $postCount = 0;
  foreach ( $story_list as $data){
    get_post_list_item_html($data, 'r');
  }
} else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
}

?>
          <div class="pager-div" style="text-align:center;border-top: 1px solid #eee;">
            <?php wpbeginner_numeric_posts_nav(get_latest_list_data($item_count, $paged, $cur_tag_obj->slug, false, false, true, $extra_param), true); ?>
          </div>
          <div class="bs-callout bs-callout-info" style="margin-top: 40px;">
            <?php require('inc/SF_STORY_BELOW-TITLE_728x90.php'); ?>
          </div>
    </main>

    <aside class="HolyGrail-right hidden-mid tnl-subject-wrap" style="background-color:rgb(230, 230, 230);">
<?php if($is_sponsor_tag): ?>
<?php
        $sp1 = dirname( __FILE__ ) . '/sponsor/tag/' . $sponsor_id . '1.php';
        if(is_file($sp1)){
            require($sp1);
        }
?>
<?php else: ?>
      <div class="tnl-panel">
        <?php require('inc/SF_STORY_SIDEBAR-HEAD_300x250.php'); ?>
      </div>
<?php endif; ?>
      <?php require('inc/follow_us.php'); ?>
<?php if($is_sponsor_tag): ?>
<?php
    $sp2 = dirname( __FILE__ ) . '/sponsor/tag/' . $sponsor_id . '2.php';
    if(is_file($sp2)){
        require($sp2);
    }
?>
<?php endif; ?>
      <div class="tnl-panel">
        <?php require('inc/hot_post.php'); ?>
      </div>
    
    </aside>

  </div>

<?php get_footer(); ?>