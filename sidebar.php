<?php 
if( is_single() ){
  $sidebar_head_style = 'background-color:#fff; border-right-style:solid; border-width: 1px; border-color: #E0E0E0; background-color:#E0E0E0;';
}else{
  $sidebar_head_style = 'background-color:#fff;';
}
?>
<style type="text/css">
.sidebar_scrol{ position:fixed;  bottom:0px; width:320px; }
.sidebar_scrol_footer{ position:fixed; bottom:181px; width:320px; }
</style>

    <aside class="HolyGrail-left-post hidden-xs" style="<?php echo $sidebar_head_style; ?>">
    </aside>
    <aside class="HolyGrail-right hidden-mid tnl-subject-wrap" style="background-color:rgb(230, 230, 230);">
        <div id="sidebarheight" style="width:320px;">
          <?php require('inc/marketing_box.php'); ?>
          <?php require('inc/follow_us.php'); ?>
          <div class="tnl-panel">
            <?php require('inc/SF_STORY_SIDEBAR-HEAD_300x250.php'); ?>
          </div>
          <div class="tnl-panel">
            <?php require('inc/hot_post.php'); ?>
          </div>
        </div>  
    </aside>
<script type="text/javascript">    
jQuery(document).ready(function(){	
    jQuery(window).scroll(function () {
        var scrollVal = jQuery(this).scrollTop(); //抓滾輪目前高度
		var sidebarheight = jQuery('#sidebarheight').height(); //抓右側元素的總高
		var mainheight = jQuery("body").height(); //抓左側元素的總高
		var navheight = jQuery("nav").height(); //抓左側元素的總高
		var footerheight = jQuery("footer").height(); //抓左側元素的總高
        //判斷IE版本
        var isIE = function(ver){
            var b = document.createElement('b')
            b.innerHTML = '<!--[if IE ' + ver + ']><i></i><![endif]-->'
            return b.getElementsByTagName('i').length === 1
        }
        if(isIE(9)){
        //我不執行JQ
        }else{
            jQuery("span.qScrollTop").text(scrollVal);
            if( mainheight-jQuery(window).height()-navheight-footerheight > 2000 ){
                if(scrollVal > sidebarheight-jQuery(window).height()-navheight ){
                    jQuery("#sidebarheight").addClass("sidebar_scrol"); //新增一個class
                }
                if(scrollVal < sidebarheight-jQuery(window).height()-navheight ){
                    jQuery("#sidebarheight").removeClass("sidebar_scrol"); //移除一個class
                }
                if(scrollVal - 50 > mainheight- jQuery(window).height() - navheight - footerheight ){
                    jQuery("#sidebarheight").removeClass("sidebar_scrol"); //移除一個class
                    jQuery("#sidebarheight").addClass("sidebar_scrol_footer"); //新增一個class
                }
                if(scrollVal > sidebarheight -jQuery(window).height()-navheight ){
                    if(scrollVal < mainheight-jQuery(window).height()-navheight-footerheight ){
                        jQuery("#sidebarheight").removeClass("sidebar_scrol_footer"); //移除一個class
                        jQuery("#sidebarheight").addClass("sidebar_scrol"); //新增一個classs
                    }
                }
            }
        }
        
    });
});  
</script>    

