<?php

function get_recent_comments(){
    $comments = array();

    $args = array(
        'status'        => 'approve',
        'number'        => 30,
        'meta_key'      => 'dsq_post_id',
     );

    $recent_comments = get_comments($args);

    foreach($recent_comments as $obj){
        if( empty( $comments[$obj->comment_post_ID] ) ){
            $comments[$obj->comment_post_ID] = $obj;
        }

        if( count($comments) >= 2 ) break;
    }

    wp_reset_query();

    return $comments;
}

function get_latest_list_data($posts_per_page = 6, $paged = 1, $tag = false, $category = false, $author = false, $pagination = false, $extra_param = false){
    //popular posts
    $data = array();
    $args = array();

    if(is_numeric($posts_per_page)) $args['posts_per_page'] = $posts_per_page;
    if(is_numeric($paged)) $args['paged'] = $paged;
    if(!empty($tag)) $args['tag'] = $tag;
    if(!empty($category)) $args['cat'] = $category;
    if(is_numeric($author)) $args['author'] = $author;
    if(is_array($extra_param)) $args = array_merge($args, $extra_param);
    // $args['category__not_in'] = unserialize(EXCLUDE_CATE);
    $my_query = new WP_Query($args);

    if($pagination) return $my_query;

    if ($my_query->have_posts()) {
        $postCount = 0;
        while ($my_query->have_posts()){
            $my_query->the_post(); 
            global $post;
            $author_id = $post->post_author;
            $art_thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'art-thumb');
            $thumbnail_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail');

            $featured_img_id = get_post_thumbnail_id($post->ID);
            $featured_img_obj = get_post($featured_img_id);
            $photo_credit = strip_tags($featured_img_obj->post_excerpt);
            $full_image_url = get_photon_url($featured_img_obj->guid);

            $is_sponsor = (get_the_author_meta('sponsor_author', $author_id) == 1);

            $is_video = false;
            $categories = get_the_category($post->ID);
            foreach($categories as $category) {
                if ( '影片' == $category->name ){
                    $is_video = true;
                    break;
                }
            }

            $desc = summarize_long_text($post->post_content, 160);
            $timestamp = strtotime(get_gmt_from_date($post->post_date));

            $data[] = array(
                'post_id'               => $post->ID,
                'permalink'             => get_permalink(),
                'title'                 => $post->post_title,
                'desc'                  => $desc,
                'excerpt'               => $post->post_excerpt,
                'timestamp'             => $timestamp,
                'art_thumb'             => get_photon_url($art_thumb_image_url[0]),
                'thumbnail'             => get_photon_url($thumbnail_image_url[0]),
                'photo_credit'          => $photo_credit,
                'full_featured_img'     => $full_image_url,
                'author_id'             => $author_id,
                'author_link'           => get_author_posts_url(get_the_author_meta( 'ID', $author_id  )),
                'author_name'           => get_the_author_meta( 'display_name', $post->post_author  ),
                'social_count'          => get_post_meta($post->ID, 'social_total_count', $single = true),
                'comment_count'         => $post->comment_count,
                'is_video'              => $is_video,
                'is_sponsor'              => $is_sponsor,
            );
        }
    }

    wp_reset_query();

    return $data;
}

function get_popular_list_with_GA($posts_per_page = 23){

    $json = json_decode(get_option( 'tnl_popular_post_list' ));

    $list = array();

    if( ! empty($json) ){

        $postCount = 0;

        foreach( $json as $data){

            if($data[0] !== 'www.thenewslens.com') continue;

            preg_match('/[0-9]+/', $data[1], $match);

            $post = get_post($match[0]);

            if($post === false) continue;

            $author_id = $post->post_author;
            $link = get_permalink($post->ID);
            $thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'art-thumb');
            $timestamp = strtotime(get_gmt_from_date($post->post_date));
            $art_thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'art-thumb');
            $thumbnail_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail');

            $is_video = false;
            $categories = get_the_category($post->ID);
            foreach($categories as $category) {
                if ( '影片' == $category->name ){
                    $is_video = true;
                    break;
                }
            }


            $list[] = array(
                'post_id'               => $post->ID,
                'permalink'             => get_permalink($post->ID),
                'title'                 => $post->post_title,
                'timestamp'             => $timestamp,
                'art_thumb'             => get_photon_url($art_thumb_image_url[0]),
                'thumbnail'             => get_photon_url($thumbnail_image_url[0]),
                'author_link'           => get_author_posts_url(get_the_author_meta( 'ID', $author_id  )),
                'author_name'           => get_the_author_meta( 'display_name', $post->post_author  ),
                'social_count'          => get_post_meta($post->ID, 'social_total_count', $single = true),
                'comment_count'         => $post->comment_count,
                'is_video'              => $is_video,
            );
            

            $postCount++;

            if( $postCount == $posts_per_page ){
                break;
            }

        }
    }

    return $list;
}


function get_mario_digest($posts_per_page = 6, $paged = 1, $pagination = false){
    $args = array(
        'post_type' => 'buzz',
        'posts_per_page' => $posts_per_page,
        'paged' => $paged,
     );

    $my_query = new WP_Query( $args );

    if ($my_query->have_posts()) {
        if(!$pagination){
            $postCount = 0;
            while ($my_query->have_posts()) :
                $my_query->the_post(); 
                global $post;

                $timestamp = strtotime(get_gmt_from_date($post->post_date));
                $url = get_post_meta( $post->ID, 'buzz_url');
                $summary = get_post_meta( $post->ID, 'buzz_summary');
                $buzz_source = esc_html( get_post_meta( $post->ID, 'buzz_source', true ) );
                $thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'art-thumb');
?>
        <div class="panel">
            <div class="media" style="width:100%;">
                <a class="pull-left" href="<?php echo $url[0]; ?>" rel="bookmark" title="<?php echo $post->post_title; ?>"><img src="<?php echo $thumb_image_url[0]; ?>" style="width: 64px; height: 64px;"></a>
                <h4 class="media-heading"><b><a href="<?php echo $url[0]; ?>" style="color:#444444;" target="_blank"><?php echo $post->post_title; ?></a></b></h4>
                <div class="media-body"><?php echo $summary[0]; ?></div>
                <span class="pull-right" style="padding-left:10px;"><abbr class="timeago" title="<?php echo date('c', $timestamp); ?>"><?php echo date('Y/m/d', $timestamp); ?></abbr></span>
                <?php if( ! empty( $buzz_source ) ): ?>
                <span class="pull-right" style="padding-left:10px;">來源: <?php echo $buzz_source; ?></span>
                <?php endif; ?>
            </div>
        </div>
<?php

                $postCount++;

            endwhile;

        } else {
            wpbeginner_numeric_posts_nav($my_query, true);
        }
    }

    wp_reset_query();
}
