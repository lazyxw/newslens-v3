<?php

// custom exercpt function
function summarize_long_text($str, $length = 60){
    $str = strip_tags($str);
    $str = str_replace("\n", ' ', $str);
    $str = preg_replace('/\[caption(.*?)\[\/caption\]/i', '', $str);

    if( $length == -1 ){
        $desc = $str;
    } else {
        $desc = mb_substr($str, 0, $length, 'UTF8');
    }

    // preg_match('/[\x{4e00}-\x{9fa5}]+.*/u', $str, $match);
    // $desc = mb_substr($match[0], 0, $length, 'UTF8');

    return $desc;
}

// pagination
function wpbeginner_numeric_posts_nav($query, $forced = false) {

    if( is_singular() and ! $forced )
        return;

    $wp_query = $query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 2 ) {
        $links[] = $paged - 1;
        // $links[] = $paged - 2;
    }

    if ( ( $paged + 1 ) <= $max ) {
        // $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<ul class="pagination">' . "\n";

    /** Previous Post Link */

    if ( $paged > 1 ){
        printf( '<li id="previous_page"%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $paged - 1 ) ), '&laquo;' );
    } else {
        echo '<li class="disabled"><a href="javascript:;">&laquo;</a></li>';
    }

    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li id="previous_page"%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li id="next_page" id="previous_page"><a href="javascript:;">...</a></li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li><a href="javascript:;">...</a></li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /** Next Post Link */
    if ( $paged < $max ){
        printf( '<li id="previous_page"%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $paged + 1 ) ), '&raquo;' );
    }else{
        echo '<li id="next_page" class="disabled"><a href="javascript:;">&raquo;</a></li>';
    }

    echo '</ul>' . "\n";

}

// get avatar url
// function get_avatar_url($get_avatar){

//     preg_match("/src=['|\"](.*?)['|\"]/i", $get_avatar, $matches);
//     //print_r($matches);
//     return get_photon_url($matches[1]);
// }

function author_add_social($authorMetaKey) {
    $authorMeta = get_the_author_meta($authorMetaKey);
    if (!empty($authorMeta)) {
        echo '<span class="td-social-wrap"><a href="' . $authorMeta . '" target="_blank"><img class="td-retina td-social-icon td-social-' . $authorMetaKey . ' td-style-1" src="' . get_template_directory_uri() . '/assets/img/icons/' . $authorMetaKey . '_square.png' .'" width="32" height="32" alt=""/></a></span>';
    }
}

function menu_user_icon()
    {
    if (is_user_logged_in())
    {
        global $current_user; get_currentuserinfo();

        $avatar_url = get_avatar_url(str_replace("'", '"', get_avatar( $current_user->ID, 16)));

        return '<img src ="' . $avatar_url . '" class = "img-rounded" style = "width : 28px;">';

    }
    else
    {
        return '<i class="glyphicon glyphicon-user pull-right" style="margin-top:5px;"></i>';
    }

}

function menu_user_email()
    {
        if (is_user_logged_in())
        {
            global $current_user; get_currentuserinfo();

            $user_email_url = $current_user->user_email;
            echo $user_email_url;
            return NULL;

        }
        else
        {
            return NULL;
        }

    }

function menu_login_logout() {

    global $current_user;
    get_currentuserinfo();

    if (is_user_logged_in())
    {
        $avatar_url = get_avatar_url(str_replace("'", '"', get_avatar( $current_user->ID, 28)));

        $log_out = '<a rel="nofollow" href="'. wp_logout_url( home_url() ) . '" title="Logout"><button type="button" class="btn btn-success"><img src ="' . $avatar_url . '" class = "img-rounded" style = "margin-top: -11px; border-radius:0; margin-bottom: -10px; width: 35px; height: auto; margin-left: -13px; margin-right: 8px; border-width: 1px; border-color: rgb(0, 131, 10);border-style: solid;">&nbsp;登出</button></a>';

        return $log_out;
    }
    else
    {
        $log_in = '<a rel="nofollow" href="http://www.thenewslens.com/wp-login.php?action=wordpress_social_authenticate&amp;provider=Facebook&amp;redirect_to=http%3A%2F%2Fwww.thenewslens.com%2Fwp-login.php%3Floggedout%3Dtrue" title="Connect with Facebook" class="wsl_connect_with_provider">
                            <button type="button" class="btn btn-success"><i class="fa fa-facebook-square"></i>&nbsp;登入</button></a>';
        return $log_in;
    }
    wp_reset_query();
}

function get_photon_url($url){


    if( class_exists( 'Jetpack' ) && method_exists( 'Jetpack', 'get_active_modules' ) && in_array( 'photon', Jetpack::get_active_modules() ) && function_exists( 'jetpack_photon_url' ) ){
        return jetpack_photon_url($url);
    } else {
        return $url;
    }
}

function get_video_icon($is_video = false){
    $icon = '<span class="glyphicon glyphicon-play-circle" style="margin: 2px; float: left;font-size: 31px;margin: 14px 0 0 -46px;color: rgba(255, 255, 255, 0.43);"></span>';
    return ($is_video)?$icon:'';
}

function get_post_list_item_html($data, $type = 'r', $show_excerpt = false){
    if($data['is_sponsor']){
        $add_style = ' style="background: rgb(240, 240, 240);"';
    }else{
        $add_style = '';
    }
?>
          <div class="post-list-item"<?php echo $add_style; ?>>
            <img src="<?php echo $data['art_thumb']; ?>" class="media-object" alt="<?php echo $data['title']; ?>" title="<?php echo $data['title']; ?>">
            <?php echo get_video_icon($data['is_video']); ?>
            <div class="post-list-item-content">
              <a href="<?php echo $data['permalink']; ?>">
                <h3><?php echo $data['title']; ?></h3>
                <h6>
<?php if($type == 'l'): // latest list ?>
                  <span style="float:left;margin-top: 3px;"><a href="<?php echo $data['author_link']; ?>"><?php echo $data['author_name']; ?></a></span>
                  <span style="display:inline-block;">在 <abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr> 發表
<?php elseif($type == 'h'): // hotest list ?>
                  <span style="float:left;margin-top: 3px;"><a href="<?php echo $data['author_link']; ?>"><?php echo $data['author_name']; ?></a></span>
                    • <?php echo $data['social_count']; ?></span> <span class="glyphicon glyphicon-share" style="margin: 2px;"></span>
<?php elseif($type == 'r'): // regular list ?>
                  <span style="float:left;margin-top: 3px;"><a href="<?php echo $data['author_link']; ?>"><?php echo $data['author_name']; ?></a></span>
                  <span style="display:inline-block;">在 <abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr> 發表  • <?php echo $data['social_count']; ?></span> <span class="glyphicon glyphicon-share" style="margin: 2px;"></span>
<?php endif; ?>
                </h6>
              </a>
<?php if( $show_excerpt and !empty($data['excerpt']) ): ?>
              <h6 class="post_excerpt"><?php echo $data['excerpt']; ?></h6>
<?php endif; ?>
            </div>
<?php if($data['is_sponsor']): ?>
            <span style="float: right;margin-top: -17px;color: #A0A0A0;font-size: 12px;">贊助文章</span>
<?php endif; ?>
          </div>
<?php
}

function strip_anchor( $str ) {
    return preg_replace('/<a href=\"(.*?)\">(.*?)<\/a>/', "\\2", $str);
}

function track_url( $url, $source = 'somewhere', $medium = 'feed', $campaign = '3rd+party' ) {
    $operator = (preg_match("/\?/i", $url)) ? "&amp;":"?";
    $format = "%s"."utm_source=%s"."&amp;utm_medium=%s"."&amp;utm_campaign=%s";
    $url_pre = $url . $operator;
    return sprintf($format,$url_pre,$source,$medium,$campaign);
}
