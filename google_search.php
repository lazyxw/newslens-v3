<?php
/*
    Template Name: google custom search
*/
?>
<?php get_header(); ?>

    <div id="left-sidebar" class="col-lg-2 visible-lg" style="">
            
    </div>

    <div id="main-content" class="col-lg-8 col-sm-8" style="padding-top:20px;min-height:300px;">
<style media="screen" type="text/css">
#main-content input[type="text"] {
    position:relative;
}

.gsc-control-cse .gs-result .gs-title, .gsc-control-cse .gs-result .gs-title * {
font-size: 18px;
line-height: 20px !important;
}

.gsc-input-box {

min-height: 32px !important;
-moz-box-shadow: inset 0 0 5px #888;
-webkit-box-shadow: inset 0 0 5px#888;
box-shadow: inner 0 0 5px #888;
border-radius: 3px;
}
.gsc-input-box-hover{
-moz-box-shadow: inset 0 0 5px #888;
-webkit-box-shadow: inset 0 0 5px#888;
box-shadow: inner 0 0 5px #888;
}
.gsc-input-box-focus {
-moz-box-shadow: inset 0 0 5px #888;
-webkit-box-shadow: inset 0 0 5px#888;
box-shadow: inner 0 0 5px #888;
}

.gsc-selected-option{
    min-width: 40px !important;
}

.gsc-search-button .gsc-search-button-v2 {
    width: auto;
    height:27px;
}

.gsst_a .gscb_a {
color: #a1b9ed;
cursor: pointer;
margin-top: 6px;
}

table.gsc-search-box {
border-style: none;
border-width: 0;
border-spacing: 0 0;
width: 97%;
margin: 0 auto;
margin-bottom: 2px;
}

.gsc-results .gsc-cursor {
font-size: 18px;
margin-left: 0 auto;
}

</style>
        <script>
          (function() {
            var cx = '004951195181085949750:gkeq2dwrqvu';
            var gcse = document.createElement('script');
            gcse.type = 'text/javascript';
            gcse.async = true;
            gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
                '//www.google.com/cse/cse.js?cx=' + cx;
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(gcse, s);
          })();
        </script>
        <gcse:searchbox></gcse:searchbox>
        <gcse:searchresults></gcse:searchresults>
    </div>

    <div id="right-top-sidebar" class="col-lg-2 col-sm-4 hidden-xs hidden-sm" style="">
        
    </div>

        


<?php get_footer(); ?>