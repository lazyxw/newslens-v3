<?php
/*
    Template Name: About Us
*/
?>

<?php get_header(); ?>
	<style>
body {
	margin: 0 0 60px;
	background-color: #f9f8f9;
	color: #333;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 13px;
	overflow: -moz-scrollbars-vertical;
}

h1 {
	margin: 40px 0 20px;
	font-size: 20px;
	text-align: center;
}

h2 {
	margin-bottom: 10px;
}

h3 {
	margin: 0 0 25px;
	font-weight: normal;
}

p {
	margin-top: 0;
	margin-bottom: 0;
}

a {
	color: #333;
	text-decoration: none;
}

ol, li {
	margin: 0;
}

.jaf-container {
	position: relative;
	padding: 0;
}

.jaf-row {
	/*zoom: 1;*/
	min-width: 320px;
}

.desc {
    padding:30px 0 10px;
    font-size:15px;
    line-height:170%;
}
.desc p {
    padding:0 3% 20px 3%;
}

.sectiontitle {
    padding: 0 3% 0 3%;
    font-size: 21px;
    margin: 40px 0 0;
    color: #428bca;
}

.folder {
	position: relative;
	float: left;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	width: 25%;
	height: 270px;
	padding: 3% 3% 10px 3%;
	cursor: pointer;
}

.folder a:hover img {
    opacity:0.4;
    border: 1px solid #428bca;
}

.folder::before {
	position: absolute;
	bottom: -1px;
	left: 50%;
	content: '';
	width: 0;
	height: 0;
	margin-left: 7px;
	border-left: 0 solid transparent;
	border-right: 0 solid transparent;
	border-bottom: 0 solid transparent;
	-webkit-transition: bottom 0.2s ease-in;
	-moz-transition: bottom 0.2s ease-in;
	transition: bottom 0.2s ease-in;
}

.folder::after {
	position: absolute;
	bottom: -1px;
	left: 50%;
	content: '';
	width: 0;
	height: 0;
	margin-left: 6px;
	border-left: 0 solid transparent;
	border-right: 0 solid transparent;
	border-bottom: 0 solid transparent;
	-webkit-transition: bottom 0.2s ease-in;
	-moz-transition: bottom 0.2s ease-in;
	transition: bottom 0.2s ease-in;
}

.folder img {
	position: relative;
	margin-top: 0;
	margin-left: 0;
	max-width: 100%;
	background-color: #ddd;
	border: 1px solid #ddd;
	-webkit-transition: all 0.2s ease-in;
	-moz-transition: all 0.2s ease-in;
	transition: all 0.2s ease-in;
	border-radius: 4px;
}

.ename {
	font-weight: bold;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
/*	-webkit-transition: all 0.2s ease-in;
	-moz-transition: all 0.2s ease-in;
	transition: all 0.2s ease-in;*/
}

.title {
	opacity: 1;	
	margin-bottom: 2px;
	color: #888;
	font-size: 12px;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	-webkit-transition: all 0.2s ease-in;
	-moz-transition: all 0.2s ease-in;
	transition: all 0.2s ease-in;
}

.app-folders-container {
	width: 100%;
	margin: 0 auto;
}

.active-tool {
	z-index: 10;
}

.active-tool::before {
	bottom: -1px;
	margin-left: -7px;
	border-left: 12px solid transparent;
	border-right: 12px solid transparent;
	border-bottom: 12px solid rgba(0,0,0,0.15);
	-webkit-transition: all 0.2s ease-in;
	-moz-transition: all 0.2s ease-in;
	transition: all 0.2s ease-in;
}

.active-tool::after {
	bottom: -1px;
	margin-left: -6px;
	border-left: 11px solid transparent;
	border-right: 11px solid transparent;
	border-bottom: 11px solid transparent;
	-webkit-transition: all 0.2s ease-in;
	-moz-transition: all 0.2s ease-in;
	transition: all 0.2s ease-in;
}

.active-tool img {
	max-width: 110%;
	margin-top: -5%;
	margin-left: -5%;
	-webkit-transition: all 0.2s ease-in;
	-moz-transition: all 0.2s ease-in;
	transition: all 0.2s ease-in;
}

.active-tool .ename {
	padding-bottom: 6px;
	-webkit-transition: all 0.2s ease-in;
	-moz-transition: all 0.2s ease-in;
	transition: all 0.2s ease-in;
}

.active-tool .title {
	opacity: 0;
	height: 2px;
	-webkit-transition: all 0.2s ease-in;
	-moz-transition: all 0.2s ease-in;
	transition: all 0.2s ease-in;
}

.folderContent {
	position: relative;
	z-index: 0;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	min-width: 320px;
	padding: 20px 0 0 60px;
	background-color: #aaa;
	border: 1px solid rgba(0,0,0,0.05);
	border-width: 1px 0;
}

.folderContent a:hover {
	text-decoration: underline;
}

.folderContent .close {
	position: absolute;
	top: 15px;
	left: 10px;
	padding: 1px 5px 3px;
	border-radius: 10px;
	background-color: rgba(0,0,0,0.3);
	color: rgba(255,255,255,0.5);
	font-weight: bold;
}

.folderContent .close:hover {
	text-decoration: none;
	background-color: rgba(0,0,0,0.6);
	color: rgba(255,255,255,0.5);
}

.multi {
/*
	-webkit-column-count: 2;
	-moz-column-count: 2;
	column-count: 2;
	-webkit-column-gap:40px;
    -moz-column-gap:40px;
    column-gap:40px;
    margin-right:50px;
*/
}

.bio {
    font-size: 15px;
    line-height: 160%;
}

.art-wrap {
	float: right;
	text-align: right;
	/*width: 30%;*/
}

.art-wrap img {
	position: relative;
	z-index: -2;
	display: block;
	width: 100%;
	max-width: 300px;
}

.clear {
	clear: both;
}

@media (max-width: 480px){

	.folder {
		height: 145px;
	}

	.folderContent {
		padding: 20px 40px;
		width: 100%;
	}
    .title {
    font-size:10px;
    }
    
    .ename {
        font-size:13px;
    }
}

@media (min-width: 481px) and (max-width: 768px){

    .ename {
        font-size:15px;
        padding:7px 0;
    }
	.folder {
		height: 224px;
	}
}

@media (max-width: 959px){

	.multi {
	/*
		-webkit-column-count: 1;
		-moz-column-count: 1;
		column-count: 1;
	*/
	}
	
    .ename {
        font-size:16px;
        padding:7px 0;
    }

	.art-wrap {
		display: none;
	}
}

@media (min-width: 960px){
    .ename {
        font-size:16px;
        padding:7px 0;
    }
    .desc, .jaf-row {
		width: 960px;
		margin: 0 auto;
	}
}
</style>
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script src="<?php echo get_template_directory_uri() . '/About/mustache.js'; ?>"></script>
	<script src="<?php echo get_template_directory_uri() . '/About/jquery.app-folders.js'; ?>"></script>
	<script src="<?php echo get_template_directory_uri() . '/About/quantize.js'; ?>"></script>
	<script src="<?php echo get_template_directory_uri() . '/About/color-thief.js'; ?>"></script>
	<script type="text/javascript">

		$(document).ready(function(){

			$.ajax({
				url: "<?php echo get_template_directory_uri() . '/data.json'; ?>",
				dataType: 'json',
				success: function(data) {
					var persons = Mustache.to_html($('#persons').html(), data);
					var details = Mustache.to_html($('#details').html(), data);
					$('.app-folders-container').html(persons+details);     
					
					$('.app-folders-container').appFolders({
						opacity: 1, 								// Opacity of non-selected items
						marginTopAdjust: true, 						// Adjust the margin-top for the folder area based on row selected?
						marginTopBase: 0, 							// If margin-top-adjust is "true", the natural margin-top for the area
						marginTopIncrement: 0,						// If margin-top-adjust is "true", the absolute value of the increment of margin-top per row
						animationSpeed: 200,						// Time (in ms) for transitions
						URLrewrite: true, 							// Use URL rewriting?
						URLbase: "./",						// If URL rewrite is enabled, the URL base of the page where used. Example (include double-quotes): "/services/"
			            internalLinkSelector: ".jaf-internal a"		// a jQuery selector containing links to content within a jQuery App Folder
					});

					// For each image:
					// Once image is loaded, get dominant color and palette and display them.
					$('.app-icon').bind('load', function (event) {
					    var image = event.target;
					    var $image = $(image);
					    var imageSection = $image.closest('.imageSection');

					    var colors = getColors(image);
					    styleBackground(colors[1], $image.parent().parent().attr('id'));
					    styleText(colors[1], colors[0],$image.parent().parent().attr('id'));

					});
				}
			});
		});

	</script>

    <div class="desc">
        <p>The News Lens 設立於2013年8月。我們是一群對於現今媒體不滿又想要做出一些改變的人。有一直在媒體產業的編輯，有多年在海外各地工作的專業經理人，還有對於網路技術有熱情的開發者，以及一群對於我們這個夢想有興趣，願意一起幫忙的實習生、朋友以及許多外稿作者。</p>
        <p>我們夢想中的媒體是除了陳述事實之外，還能夠提供多元、不同方向的觀點，並為智慧手機、平板、電腦等不同平台提供各自適合閱讀內容。也讓社交網路世代的使用者，能夠更輕鬆的分享、討論和參與他們有興趣的議題。</p>
    </div>
	<div class="app-folders-container">
		<script id="persons" type="text/x-mustache">
			{{#data}}
				{{#first}}
				<div class='jaf-row jaf-container'>
				{{#section}}<h3 class='sectiontitle'>{{section}}</h3>{{/section}}
				{{/first}}
					<div class='folder' id='{{id}}'>
						<a href='#'>
							<img class='app-icon' src='<?php echo get_template_directory_uri(); ?>/About/{{image}}'>
							<p class='ename'>{{name}}</p>
							<h2 class='title'>{{title}}</h2>
						</a>
					</div>
				{{#last}}
					<br class='clear'>
				</div>
				{{/last}}
			{{/data}}

			</div>
		</script>

		<script id="details" type="text/x-mustache">
			{{#data}}
				<div class='folderContent {{id}}'>
					<div class='jaf-container'>
						<div>
							<div class='art-wrap'>
								<img src='<?php echo get_template_directory_uri(); ?>/About/{{image}}'>
							</div>
							<h2><span class="primaryColor">{{fullname}}</span></h2>
							<h2 class="secondaryColor">{{title}}</h2>
							<div class='multi'>
								<p class="primaryColor bio">{{bio}}
										<br><br>								
										<a href="{{email}}"><img src='{{email_pic}}'></a>			
									    <a href="{{facebook}}"><img src='{{facebook_pic}}'></a>		
									    <a href="{{twitter}}"><img src='{{twitter_pic}}'></a>										
										<a href="{{blogger}}"><img src='{{blogger_pic}}'></a>	
										<a href="{{wordpress}}"><img src='{{wordpress_pic}}'></a>	
										<a href="{{googleplus}}"><img src='{{googleplus_pic}}'></a>			
										<a href="{{linkedin}}"><img src='{{linkedin_pic}}'></a>	
										<a href="{{youtube}}"><img src='{{youtube_pic}}'></a>	
								</p>
							</div>
						</div>
						<br class='clear'>
					</div>
					<a href="#{{id}}" class="close">&times;</a>
				</div>
			{{/data}}
		</script>
	</div>

<?php get_footer(); ?>