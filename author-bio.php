<?php
$html_avatar = get_avatar_url(get_avatar( $post->post_author, 100 ));
$author_type = get_the_author_meta('reprint');
if( 1 == $author_type ){
  $reprint_mark = '<div class="label label-default" style="margin: 0px 4px 0px 9px;">合作轉載</div>';
} else {
  $reprint_mark = '';
}

?>
      <div class="author-info">
        <img src="<?php echo $html_avatar; ?>" class="" alt="" style="">
        <div>
          <h1><?php coauthors(' ', ' '); ?></h1>
          <div class="" ><?php echo $reprint_mark; ?><p style="margin-left:0px;"><?php echo str_replace("\n", '<br>', get_the_author_meta( 'description' )); ?></p>
          </div>
          <div class="author-social">
<?php
author_add_social('facebook');
author_add_social('googleplus');
author_add_social('twitter');
author_add_social('linkedin');
author_add_social('youtube');
?>
          </div>
          <div class="author-website">
<?php
                $authorWebsite = get_the_author_meta('url');
                if (!empty($authorWebsite)) {
                    echo '<a href="' . $authorWebsite . '" target="_blank">' . $authorWebsite . '</a>';
                }
?>
          </div>
          <div>更多 <?php coauthors_posts_links(' ', ' '); ?> 的文章</div>
        </div>
      </div>
