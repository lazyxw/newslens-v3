<?php
/*
    Template Name: official blog list
*/

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

$item_count = 10;
$cate_id = get_query_var('cat');
$story_list = get_latest_list_data($item_count, $paged, false, $cate_id);

?>
<style type="text/css">
.blog_date_box{ width: 60px;height: 60px;float: left; border-radius: 2px;background-color:#428bca;}
.blog_date_box_top{ width: 60px;height: 22px; clear:left;text-align:center;border-bottom-style:solid; border-bottom-width:medium;border-bottom-color:#ffffff; }
    .blog_date_box_top_font1{ font-size:12px;color:#ffffff;font-family: 'Segoe UI', Tahoma, Helvetica, Sans-Serif;}
    .blog_date_box_top_font2{ font-size:14px;color:#ffffff;font-family: 'Segoe UI', Tahoma, Helvetica, Sans-Serif;}
.blog_date_box_bottom{ width: 60px;height: 38px; clear:left; }
    .blog_date_box_bottom_place{ width: 60px;clear:left;text-align:center;overflow-x: hidden; }
    .blog_date_box_bottom_font{ font-size:35px;color:#ffffff;margin-top:-5px;}
.post-list-item div h7{
  line-height: 1.4em;
  font-size: 16px;
  color: #292424;
 font-family: inherit;
 font-weight: 500;        
}  
blog-read-more {text-align:right;}    
</style>
<?php get_header(); ?>

  <div class="HolyGrail-body-post" style="margin-top: 50px !important;">

    <main class="HolyGrail-content-post" style="margin-top:0;">
       
      <div class="category-nav" style="">
        <div class="col-lg-12">
          <div class="newest-title">
            <h1 style="padding: 15px 31px 10px 0px; max-width:250px; color:rgb(117, 117, 117);">官方部落格</h1>
          </div>

        </div>
        <div class="clearfix"></div>
      </div>

      <div class="category-nav" style="border-bottom: 0px;
      min-height: 8px !important;
      margin-top: -17px;
      margin-bottom: 10;
      color: #ADADAD;">
        <span style="float:right; margin-right:25px;">第 <?php echo $paged; ?> 頁 </span>
      </div>
<?php
if ( count($story_list) > 0 ) {
  $postCount = 0;
  foreach ( $story_list as $data){
?>
      <div class="post-list-item">
            <div class="media-object blog_date_box">
               <div class="blog_date_box_top">
                   <span class="blog_date_box_top_font1"><?php echo date('Y', $data['timestamp']); ?></span>
                   <span class="blog_date_box_top_font2"><?php echo date('M', $data['timestamp']); ?></span>
               </div>
               <div class="blog_date_box_bottom">
                   <div class="blog_date_box_bottom_place">
                       <div class="blog_date_box_bottom_font"><?php echo date('d', $data['timestamp']); ?></div>
                    </div>
               </div>
            </div>
            <div class="post-list-item-content">
              <a href="<?php echo $data['permalink']; ?>">
                <h3><?php echo $data['title']; ?></h3>
                </a><h6><a href="<?php echo $data['permalink']; ?>">
                  <span style="float:left;margin-top: 3px;"></span></a><a href="<?php echo $data['author_link']; ?>"><?php echo $data['author_name']; ?></a>
                  <span style="display:inline-block;">在 <abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr> 發表  • </span> <span class="glyphicon glyphicon-share" style="margin: 2px;"></span>
                </h6>
                <div class="post-cover" style=" background-size: 100%; background-image: url('<?php echo $data['full_featured_img']; ?>'); height:150px;width:auto;margin-left:80px;">
                </div>
                <div style="margin-left:80px; margin-top:10px;">
                <h7 class="post_excerpt" style="color:#333"><?php echo $data['desc']; ?></h7>
                <p></p>
                <a href="<?php echo $data['permalink']; ?>/" class="button" style="font-size:14px;">Read More</a>
                </div>    
            </div>
      </div>
<?php
  }
} else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
}

?>
          <div class="pager-div" style="text-align:center;border-top: 1px solid #eee;">
            <?php wpbeginner_numeric_posts_nav(get_latest_list_data($item_count, $paged, false, $cate_id, false, true), true); ?>
          </div>


  
    </main>
    <aside class="HolyGrail-left-post hidden-xs" style="background-color:#fff;">
    </aside>
    <aside class="HolyGrail-right hidden-mid tnl-subject-wrap" style="background-color:#ffffff;">
          <img src="<?php echo get_template_directory_uri(); ?>/img/blog_img.png" class="img-responsive" width="200" height="auto" alt="" style="min-width:160px; max-width: 200px; margin: 0px auto; line-height: 20px;padding: 14px 0 0 0;float:left;">
    </aside>

  </div>

<?php get_footer(); ?>