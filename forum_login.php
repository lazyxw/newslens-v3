

<?php
/*
  Template Name: Forum Login
*/
?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<style type="text/css">
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed,
figure, figcaption, footer, header, hgroup,
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font-size: 100%;
  font: inherit;
  vertical-align: baseline;
}

article, aside, details, figcaption, figure,
footer, header, hgroup, menu, nav, section {
  display: block;
}

body {
  line-height: 1;
}

ol, ul {
  list-style: none;
}

blockquote, q {
  quotes: none;
}

blockquote:before, blockquote:after,
q:before, q:after {
  content: '';
  content: none;
}

table {
  border-collapse: collapse;
  border-spacing: 0;
}
html{height:100%;}
body {
  font: 13px/20px 'Lucida Grande', Tahoma, Verdana, sans-serif;
  background-repeat: no-repeat;
  color: #404040;

  /* Note: This gradient may render differently in browsers that don't support the unprefixed gradient syntax */

/* IE10 Consumer Preview */ 
background-image: -ms-linear-gradient(top right, #097DA1 0%, #00A3EF 100%);

/* Mozilla Firefox */ 
background-image: -moz-linear-gradient(top right, #097DA1 0%, #00A3EF 100%);

/* Opera */ 
background-image: -o-linear-gradient(top right, #097DA1 0%, #00A3EF 100%);

/* Webkit (Safari/Chrome 10) */ 
background-image: -webkit-gradient(linear, right top, left bottom, color-stop(0, #097DA1), color-stop(1, #00A3EF));

/* Webkit (Chrome 11+) */ 
background-image: -webkit-linear-gradient(top right, #097DA1 0%, #00A3EF 100%);

/* W3C Markup, IE10 Release Preview */ 
background-image: linear-gradient(to bottom left, #097DA1 0%, #00A3EF 100%);
  }
.about {
  margin: 70px auto;
  padding: 12px;
  width: 260px;
  font: 10px/18px 'Lucida Grande', Arial, sans-serif;
  color: #666;
  text-align: center;
  text-shadow: 0 1px rgba(255, 255, 255, 0.25);
  background: #5cb85c;
  border-radius: 4px;
  font-family: Helvetica, Heiti TC, Segoe UI, "微軟正黑體";
  font-size: 22px;
  line-height: inherit;
}
.container {
  margin: 80px auto;
  width: 310px;
  box-shadow: 0 4px 24px 6px rgba(0, 0, 0, 0.3);
  border-radius: 30px;
}

.login {
  position: relative;
  margin: 0 auto;
  width: 310px;
  background: white;
  border-radius: 3px;
  -webkit-box-shadow: 0 0 200px rgba(255, 255, 255, 0.5), 0 1px 2px rgba(0, 0, 0, 0.3);
  box-shadow: 0 0 200px rgba(255, 255, 255, 0.5), 0 1px 2px rgba(0, 0, 0, 0.3);
  border-radius: 30px;
}
 .login h1 {
  text-align: center;
  background: white;
  border-radius: 3px 3px 0 0;
  border-radius: 30px;
} 
</style>

  <section class="container">
    <div class="login">
      <h1><img src="http://www.thenewslens.com/wp-content/uploads/2014/08/forum_logo1.png" style="margin-top:55px;"></h1>
        <a rel="nofollow" href="http://www.thenewslens.com/wp-login.php?action=wordpress_social_authenticate&amp;provider=Facebook&amp;redirect_to=http%3A%2F%2Fwww.thenewslens.com%2Fsso%2F%3Fsso%3Dbm9uY2U9ODkwMDllMTYxODg4ZGMzOWY1OGRmNDQ4Y2QzNWY5YTE%253D%26sig%3D85f49cac750be672deb0bf6daad370719b6c297d934281fe9148c6a3ed956875" title="Connect with Facebook" class="wsl_connect_with_provider">
            <img src="http://www.thenewslens.com/wp-content/uploads/2014/08/fb_login.png" style="width:300px;text-align: center;margin-left: 3px;">
        </a>
    </div>
  </section>

  <section class="about">
    <p class="about-links">
      <a href="http://www.thenewslens.com" target="_parent" style="color: #fff;text-decoration: none;">回到關鍵評論網</a>
    </p>
  </section>



