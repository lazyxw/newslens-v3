/*
 * IE Alert! jQuery plugin
 * Version 2.1
 * Author: David Nemes | @nmsdvid
 * http://nmsdvid.com/iealert/
 */

(function ($) {
    function initialize($obj, support, title, text, upgradeTitle, upgradeLink, overlayClose, closeBtn) {
    
    
        var cookie_exp  = new Date();
        var cookie_opt  = { path: "/" };
        var cookie_name = 'ie_alert';

        cookie_exp.setTime(cookie_exp.getTime() + 31536000000);
        cookie_opt.expires = cookie_exp;
    
        var panel = '<div class="text-center" style="width:100%; height:100%; padding:0;"><div class="panel" style="margin: auto auto; max-width:400px; max-height:400px;"><div class="panel-heading" style="border-bottom-style:solid; border-width:1px; border-color: #F1F1F1;"><img src="http://www.thenewslens.com/wp-content/themes/newslens_v3/img/logo.png" style="max-width:200px; height:auto; margin: 0 auto;"></div><div class="panel-body "><h3 style="font-size:18px; margin-bottom:30px;">親愛的讀者～您的IE瀏覽器已經不堪使用...</h3><p style="font-size:15px; line-height: 18px; margin:30px;">為了讓您在瀏覽本網站時更能心領神會一目十行，建議立馬將您的瀏覽器升級。</p><button type="button" class="btn btn-default btn-close-alert">不要更新</button><button style="margin-left:5px;" type="button" class="btn btn-info btn-upgrade">馬上更新瀏覽器</button></div></div></div>';
        var overlay = $("<div id='ie-alert-overlay'></div>");
        var iepanel = $("<div id='ie-alert-panel'>" + panel + "</div>");

        var docHeight = $(document).height();

        overlay.css("height", docHeight);

        function active() {
            $obj.prepend(iepanel);
            $obj.prepend(overlay);

            var cHeight = $('.ie-c').css('height');
            $('.ie-l').css('height', cHeight);
            $('.ie-r').css('height', cHeight);
            var uWidth = $('.ie-u-c').width();
            $('.ie-u').css('margin-left', -(uWidth / 2 + 14));
            var iePanel = $('#ie-alert-panel');
            var ieOverlay = $('#ie-alert-overlay');
            var ieBtn = $(".btn-close-alert");
            var ieUpgBtn = $(".btn-upgrade");

            ieUpgBtn.click(function (e) {
                location.href = upgradeLink;
            });

            if (closeBtn === false) {
                ieBtn.css('background-position', '-145px -58px');
                ieBtn.click(function (e) {
                   e.preventDefault();
                });
            } else {
                ieBtn.click(function () {
                    jQuery.cookie(cookie_name, '0', cookie_opt);
                    iePanel.fadeOut(100);
                    ieOverlay.fadeOut("slow");
                });
            }

            if (overlayClose === true) {
                ieOverlay.click(function () {
                    iePanel.fadeOut(100);
                    $(this).fadeOut("slow");
                });
            }

            if (ie === 6) {
                iepanel.addClass("ie6-style");
                overlay.css("background", "#d6d6d6");
                $obj.css("margin", "0");
            }
        }

        if(jQuery.cookie(cookie_name) === null ) {
            if (support === "ie9") {            // the modal box will appear on IE9, IE8, IE7, IE6
                if (ie < 10) {
                    active();
                }
            } else if (support === "ie8") {     // the modal box will appear on IE8, IE7, IE6
                if (ie < 9) {
                    active();
                }
            } else if (support === "ie7") {     // the modal box will appear on IE7, IE6
                if (ie < 8) {
                    active();
                }
            } else if (support === "ie6") {     // the modal box will appear only on IE6 and below
                if (ie < 7) {
                    active();
                }
            }
        }

    }

    ; //end initialize function

    $.fn.iealert = function (options) {

        var defaults = {
            support:"ie8",
            title:"親愛的讀者～您的IE瀏覽器已經不堪使用...",
            text:"為了讓您在瀏覽本網站時更能心領神會一目十行，建議立馬將您的瀏覽器升級。",
            upgradeTitle:"升級",
            upgradeLink:"http://browsehappy.com/",
            overlayClose:false,
            closeBtn: true
        };


        var option = $.extend(defaults, options);

        return this.each(function () {
        	
	    	ie = (function(){
	 
			    var undef,
			        v = 3,
			        div = document.createElement('div'),
			        all = div.getElementsByTagName('i');
			    
			    while (
			        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
			        all[0]
			    );
			    
			    return v > 4 ? v : undef;
	    
	    	 }());

	    	 // If browser is Internet Explorer
             if (ie >= 5) {
                var $this = $(this);
                initialize($this, option.support, option.title, option.text, option.upgradeTitle, option.upgradeLink, option.overlayClose, option.closeBtn);
             }

        });

    };
})(jQuery);
