var lang = new LangChansform();

jQuery(document).ready(function() {
    jQuery("abbr.timeago").timeago();
    jQuery().UItoTop({ easingType: 'easeOutQuart' });

    if(lang.language == 'zh_CN'){
        lang.transformLan('zh_CN');
        jQuery('#btn-s').addClass('active');
        jQuery('#btn-t').removeClass('active');
    }

    jQuery("body").iealert({
        support:"ie8",
        title:"親愛的讀者～您的IE瀏覽器已經不堪使用...",
        text:"為了讓您在瀏覽本網站時更能心領神會一目十行，建議立馬將您的瀏覽器升級。",
        upgradeTitle:"升級",
        upgradeLink:"http://browsehappy.com/",
        overlayClose:true,
        closeBtn: true        
    });
});


jQuery('#btn-s').click(function() {
    lang.transformLan('zh_CN');
    jQuery('#btn-s').addClass('active');
    jQuery('#btn-t').removeClass('active');
});

jQuery('#btn-t').click(function() {
    lang.transformLan('zh_TW');
    jQuery('#btn-t').addClass('active');
    jQuery('#btn-s').removeClass('active');
});

// insert warning message for censored image.
jQuery('#entry-content').find('.censorship').each(function(){
    var thisObj = jQuery(this);
    var thisMaskObj = jQuery('<a href="#" class="co">畫面帶有可能令人不適的內容，請斟酌點閱。</a>');
    coverIt(thisObj, thisMaskObj);
});

function coverIt(imageObj, maskObj){
    var thisObj = imageObj;
    var thisMaskObj = maskObj;
    thisMaskObj.css({
        "z-index":"99",
        "position":"relative",
        "top":thisObj.height() / 2,
        "font-size":"24px",
        "font-weight":"600",
        "display":"block",
        "width":"100%",
        "text-align":"center",
        "margin-bottom":"20px",
        "background":"#428bca",
        "color":"#fff"
    });

    thisObj.css({
        "-webkit-filter":"blur(30px)", 
        "-moz-filter":"blur(30px)", 
        "filter":"blur(30px)",
        "cursor":"pointer"
    });

    thisMaskObj.on('click', function(e){
        thisObj.css({"-webkit-filter":"", "-moz-filter":"", "filter":"", "cursor":""});
        thisMaskObj.remove();
        thisMaskObj = jQuery();
        return false;
    });

    thisObj.on('click', function(e){
        if(thisMaskObj.length){
            thisObj.css({"-webkit-filter":"", "-moz-filter":"", "filter":"", "cursor":""});
            thisMaskObj.remove();
            thisMaskObj = jQuery();
            return false;
        }
    });

    thisObj.before( thisMaskObj );
}
