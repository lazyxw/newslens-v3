jQuery(function ($) {
/**
 * modified from theatlantic's in focus ( http://www.theatlantic.com/ )
 *
 */
var InFocus = new Object();

/**
 * Enable navigation by the j/k or <--/--> keys
 * Applied at the bottom of InFocus Entries
 */
InFocus.enableKeyboardNavigation = function() { 
  var scrollIndexMin = 0; // min index for navigating, -1 is the text above the first image
  var scrollIndex = scrollIndexMin-1; // Which image we are currently on, scrollIndexMin-1 is no image
  var scrollIndexMax = InFocus.getAllPhotos().length-1; // -1 because indexes start at 0 not 1
  
  $(document).keydown(function(event) {
    //if ($('input:focus').length === 0) { // Don't scroll page if they are typing into an input field
      switch(event.which) {
        case 74: // j
        case 39: // --> (right arrow key)
          // Move Down
          if(scrollIndex < scrollIndexMax) {
            scrollIndex++;
            InFocus.goToImage(scrollIndex);
          }
        break;

        case 75: // k
        case 37: // <-- (left arrow key)
          // Move Up
          if(scrollIndex > scrollIndexMin) {
            scrollIndex--;
            InFocus.goToImage(scrollIndex);
          }
        break;
      }
    //}
  });
}

/**
 * Move browser to a particular image name
 */
InFocus.goToImage = function(imageNumber) { 
  var location;
  if (imageNumber === -1) {
    location = InFocus.getAllPhotos()[0];
  } else {
    location = InFocus.getAllPhotos()[imageNumber];
  }
  $(window).scrollTop($(location).offset().top - 60);
}


/**
 * Returns an array of all visible images in the current gallery
 */
InFocus.getAllPhotos = function() {
  if (InFocus._allPhotos == undefined) {    
    InFocus._allPhotos = $('#zh-content img');
  }
  return InFocus._allPhotos;
}

/**
 * Legacy helpter function for getting elements by ID
 */
InFocus.getEl = function(elementId) {
  return document.getElementById(elementId);
}

InFocus.enableKeyboardNavigation();

});



