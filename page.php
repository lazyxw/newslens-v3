<?php get_header(); ?>

  <div class="HolyGrail-body-post">
    <main class="HolyGrail-content-post" style="margin-top:0;">
   
    <div class="col-lg-12" style="margin-top:30px;">
      <?php while ( have_posts() ) : the_post(); ?>

          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
              <div class="entry-content">
                  <?php the_content(); ?>
              </div>
          </article><!-- #post -->

      <?php endwhile; ?>
    </div>

    <div class="clear-fix"></div>
    </main>
    <aside class="HolyGrail-left-post hidden-xs" style="background-color:#fff;"></aside>
    
  </div>

<?php get_footer(); ?>