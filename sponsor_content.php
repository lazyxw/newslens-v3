<?php
/*
    Template Name: sponsor content
*/
?>
<?php get_header(); ?>

  <div class="HolyGrail-body-post">

<?php

$content = get_the_content();
$content = '<a id="dd_start"></a>' . $content . "\n<a id=\"dd_end\"></a>";

$featured_img_id = get_post_thumbnail_id($post->ID);
$featured_img_obj = get_post($featured_img_id);
$photo_credit = strip_tags($featured_img_obj->post_excerpt);
$cover_photo = get_photon_url($featured_img_obj->guid);

$sponsor_id = get_the_author_meta('user_nicename', $post->post_author);
?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <main class="HolyGrail-content-post">
    <div class="post-cover" style="background-image: url('<?php echo $cover_photo; ?>');">
      <div class="post-social-horizon">
        <?php require('inc/share.php'); ?>
      </div>
    </div>
    <div class="photo-credit" style=""><?php echo $photo_credit; ?></div>

    <div>
        <h1 class="text-center"> <?php the_title(); ?></h1>
        <h5 class="text-center" style="margin-top: 25px; margin-bottom: 24px;"> <span class="label label-primary ">Sponsor Content</span></h5>
        <h5 class="post-info text-center"><?php coauthors_posts_links(' ', ' '); ?> <time class="entry-date" datetime="<?php echo date(DATE_W3C); ?>" ><?php the_time(get_option('date_format')) ?></time> 發表於
        <?php
        $categories = get_the_category();
        $cate_container = array();

        if($categories){
            foreach($categories as $category) {
                if ($category->slug != 'featured' and $category->slug != 'news') { //ignore the featured category name
                    $cate_name = $category->name;

                    $parent = get_category($category->category_parent);

        ?>
                    <span><a class="post-cat-box" href="<?php echo get_category_link($category->cat_ID); ?>">&#149;&#32;<?php echo $cate_name; ?></a></span>
        <?php
                    $cate_container[] = $category->name;
                }
            }
        }
        ?>
        </h5>

        <div class="tnl-content tnl-subject-wrap">

        <?php 
            $en_content = get_post_meta( get_the_ID(), 'en_content', true );

            if(!empty($en_content)){
        ?>
        <div style="padding-top: 29px;">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#zh-content" data-toggle="tab">中文</a></li>
                <li><a href="#en-content" data-toggle="tab">English</a></li>
            </ul>
        </div>
        <?php
            }
        ?>

        <div class="clearfix"></div>



        <div id="entry-content" class="media-body entry-content tab-content" style="font-size:17px; padding:10px">
            <div class="tab-pane fade active in" id="zh-content">
                <?php

                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]&gt;', $content);
                    $content_container = explode("</p>", $content);

                    $param_count = count($content_container);

                    foreach ($content_container as $key => $value) {



                        echo $value . '</p>';
                    }

                    require('inc/feature_story_nav.php');
                ?>
            </div>
            <?php if(!empty($en_content)): ?>
            <div class="tab-pane fade" id="en-content">
                <?php
                $en_content = apply_filters('the_content', $en_content);
                $en_content = str_replace(']]>', ']]&gt;', $en_content);
                    
                echo $en_content; 
                ?>
            </div>
            <?php endif;?>
        </div>
      </div>

<?php get_template_part( 'author-bio' ); ?>

<?php if($sponsor_id == 'sales'): ?>
    <div class="bs-callout bs-callout-info">
    <?php require('inc/SF_STORY_BELOW-CONTENT_728x90.php'); ?>
    </div>
<?php endif; ?>

    <a name="comment-panel"></a>
    <div style="width:80%;margin:0 auto;">
    <?php comments_template(); ?>
    </div>

    </main>
    </article>

    <aside class="HolyGrail-right hidden-mid tnl-subject-wrap" style="background-color:rgb(230, 230, 230);">
        <?php require('inc/feature_story_nav_sidebar.php'); ?>
<?php
    $sp1 = dirname( __FILE__ ) . '/sponsor/author/' . $sponsor_id . '1.php';
    if(is_file($sp1)){
        require($sp1);
    } else {
?>
      <div class="tnl-panel">
        <?php require('inc/SF_STORY_SIDEBAR-HEAD_300x250.php'); ?>
      </div>

<?php
    }

    require('inc/follow_us.php');

    $sp2 = dirname( __FILE__ ) . '/sponsor/author/' . $sponsor_id . '2.php';
    if(is_file($sp2)){
        require($sp2);
    }
?>

      <div class="tnl-panel">
        <?php require('inc/hot_post.php'); ?>
      </div>
    
    </aside>
  </div>
<?php get_footer(); ?>