<?php

if( is_home() ){
  $footer_style = '';
}else{
  $footer_style = ' style="border-top-color: rgb(230, 230, 230); border-top-style: solid;border-top-width: 1px;"';
}

?>

<!--手機 web 網告CSS and JQ開始-->
<style type="text/css">
#bottombar {
background-color:#000;
position: fixed; !important;
width: 100%;
bottom: 0px;
z-index: 5;
height: 50px;
border: 1px solid #999999;
background-repeat:repeat-x;
color:#FFFFFF;
_position:absolute;
_top:expression(document.body.scrollTop+document.body.clientHeight-this.clientHeight);
}
.none {display:none}
</style>
<script type="text/javascript">    
jQuery(document).ready(function(){  
        var footerwidth = jQuery("footer").width(); //抓左側元素的總高  
    if(footerwidth < 760 ){
      jQuery("#bottombar").removeClass("none"); //移除一個class 讓廣告顯示
    }
    if(footerwidth > 760 ){
      jQuery("#bottombar").addClass("none"); //新增一個class 讓廣告消失
    }
});
</script>    
<!--手機 web 網告CSS and JQ結束-->
<div id="bottombar" class="none">
  <?php require('inc/TNL_MOBILE_320x50.php'); ?>
</div>
<!--手機版網站廣告-->
  <footer id="tnl-footer">

    <div style="border-top-style: solid;border-top-width: 4px;border-top-color: #E0E0E0;border-bottom-style: solid;border-bottom-width: 2px;border-bottom-color: #2E2D2D;"></div>

    <div class="text-center" style=" background-image: url('<?php echo get_template_directory_uri(); ?>/img/tweed.png'); height:90px;">
      <form action="http://thenewslens.us3.list-manage.com/subscribe/post?u=a0d4f386a600c0574144f8895&amp;id=61a0b84fff" onsubmit="return newsletter_check(this)" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
        <label class="visible-xs" for="mce-EMAIL" style="color:#fff; padding-top:5px; margin-bottom:-13px;">訂閱關鍵評論網的電子報</label><br>
        
        <div class="mailchimp" style="">
          <label class="hidden-xs" style="color:#fff; padding-top:5px; margin-bottom:-13px; float:left;">訂閱關鍵評論網的電子報</label>
          <input type="email" value="" name="EMAIL" class="email" style="float: left;
padding: 5px;
width: 210px;
min-height: 34px;" id="mce-EMAIL" placeholder="請在這裡輸入您的電子郵件信箱" required="">
            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
            <div style="position: absolute; left: -5000px;"><input type="text" name="b_a0d4f386a600c0574144f8895_61a0b84fff" value=""></div>
          </div>
        <div><input type="submit" value="訂閱" name="subscribe" id="mc-embedded-subscribe" class="btn btn-success" style="float: left;
border-top-left-radius: 0px;
border-bottom-left-radius: 0px;
margin-left: -4px;"></div>
      </form>
    </div>


    <div class="HolyGrail-body" style="min-height:71px; background-color:#fff;border-top-style: solid; border-top-width: 4px; border-top-color: #E0E0E0; ">
      <div class="HolyGrail-left" style="">
        <ul class="footer-list-inline" style="padding-left:0; color:#727272; margin:0 auto; width:330px; float:none;">
          <li><a href="/news-lens%E9%97%9C%E9%8D%B5%E8%A9%95%E8%AB%96%E7%B6%B2%E5%BE%B5%E4%BA%BA%E5%95%9F%E4%BA%8B/">徵人</a></li>
          <li><a href="/authors/">專欄作者</a></li>
          <li><a href="/aboutus/">關於我們</a></li>
          <li><a href="/privacy/">隱私權聲明</a></li>
          <li><a href="/contactus/">聯絡我們</a></li>
          <li><a href="/topic/official-blog/">Blog</a></li>
        </ul>
      </div>
      <div style="margin-top:0; padding:25px 15px; font-family: 'Radley'; font-size:16px;" class="text-center HolyGrail-content"><div><span style="background-color:#fff; padding-left:10px;"><img src="<?php echo get_template_directory_uri(); ?>/img/icon.png" style="width:17px; height:auto; margin: -3px 5px 0 0px;"><span>NEWS WORTH KNOWING, VOICES WORTH SHARING</span></span> <span style="background-color:#fff; display:inline-block; padding-right:10px; padding-left:5px;">分享觀點從這開始</span><hr class="visible-lg" style="margin-top: -12px;"></div><hr class="visible-xs" style="margin-bottom:-35px;"></div>
      <div class="HolyGrail-right text-center" style="color: #727272;
padding: 29px 10px 15px 0px; font-size: .85em;">
        Copyright © 2014 The News Lens 關鍵評論
        </div>
    </div>


  </footer>
<?php wp_footer(); ?>
<?php require('inc/social_media_api_footer.php'); ?>
<?php require('inc/marketing_footer.php'); ?>

<script type="text/javascript" src="http://ad.sitemaji.com/ysm_thenewslen.js"></script>
<IFRAME STYLE="border:0px;width:0px;height:0px;" SRC="http://ad.yieldmanager.com/pixel?id= 959528&t=2"></IFRAME>

</body>
</html>