<?php
/*
    Template Name: photo gallery
*/
?>
<?php get_header(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/dist/js/photo_nav.js"></script>

  <div class="HolyGrail-body-post">
    <main class="HolyGrail-content-post" style="margin-top:0;">
   
    <div class="col-lg-12" style="margin:0px; padding:0;">
<?php

$content = get_the_content();
$content = '<a id="dd_start"></a>' . $content . "\n<a id=\"dd_end\"></a>";
$featured_img_id = get_post_thumbnail_id($post->ID);
$featured_img_obj = get_post($featured_img_id);
$photo_credit = strip_tags($featured_img_obj->post_excerpt);
$cover_photo = get_photon_url($featured_img_obj->guid);

?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="post-cover" style="background-image: url('<?php echo $cover_photo; ?>');">
      <div class="post-social-horizon">
        <?php require('inc/share.php'); ?>
      </div>
    </div>
    <div class="photo-credit" style=""><?php echo $photo_credit; ?></div>

    <div>
        <h1 class="text-center"><?php the_title(); ?></h1>
        <h5 class="post-info text-center"><?php coauthors_posts_links(' ', ' '); ?> <time class="entry-date" datetime="<?php echo date(DATE_W3C); ?>" ><?php the_time(get_option('date_format')) ?></time> 發表於
        <?php
        $categories = get_the_category();
        $cate_container = array();

        if($categories){
            foreach($categories as $category) {
                if ($category->slug != 'featured' and $category->slug != 'news') { //ignore the featured category name
                    $cate_name = $category->name;

                    $parent = get_category($category->category_parent);

        ?>
                    <span><a class="post-cat-box" href="<?php echo get_category_link($category->cat_ID); ?>">&#149;&#32;<?php echo $cate_name; ?></a></span>
        <?php
                    $cate_container[] = $category->name;
                }
            }
        }
        ?>
        </h5>
        <div class="text-center">您可以使用 ←/→ 按鍵來瀏覽圖片</div>
        <div class="tnl-content tnl-subject-wrap photo-gallery">

        <?php 
            $en_content = get_post_meta( get_the_ID(), 'en_content', true );

            if(!empty($en_content)){
        ?>
        <div style="padding-top: 29px;">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#zh-content" data-toggle="tab">中文</a></li>
                <li><a href="#en-content" data-toggle="tab">English</a></li>
            </ul>
        </div>
        <?php
            }
        ?>

        <div class="clearfix"></div>



        <div id="entry-content" class="media-body entry-content tab-content" style="font-size:17px; padding:10px">
            <div class="tab-pane fade active in" id="zh-content">
                <?php

                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]&gt;', $content);

                    echo $content;

                    
                    require('inc/feature_story_nav.php');
                    require('inc/tag_list.php');
                    require('inc/fan_page_like.php');
                ?>
            </div>
            <?php if(!empty($en_content)): ?>
            <div class="tab-pane fade" id="en-content">
                <?php
                $en_content = apply_filters('the_content', $en_content);
                $en_content = str_replace(']]>', ']]&gt;', $en_content);
                    
                echo $en_content; 
                ?>
            </div>
            <?php endif;?>
        </div>

      </div>

<?php get_template_part( 'author-bio' ); ?>

    <div class="bs-callout bs-callout-info">
    <?php require('inc/SF_STORY_BELOW-CONTENT_728x90.php'); ?>
    </div>

    <a name="comment-panel"></a>
    <div style="width:80%;margin:0 auto;">
    <?php comments_template(); ?>
    </div>

    </article>
    </div>

    <div class="clear-fix"></div>
    </main>
    
  </div>

<?php get_footer(); ?>
