<?php
/*
    Template Name: author list
*/
function get_authors_has_post(){
  global $wpdb;
  $min_posts = 1; // Make sure it's int, it's not escaped in the query

  $author_ids = $wpdb->get_col("SELECT `post_author` FROM
      (SELECT `post_author`, COUNT(*) AS `count` FROM {$wpdb->posts}
          WHERE `post_type`= 'post' and `post_status`='publish' GROUP BY `post_author`) AS `stats`
      WHERE `count` >= {$min_posts} ORDER BY `count` DESC, post_author;");
  // Do what you want to $author_ids from here on...
  return $author_ids;
}

$active_class = ' class="active"';

if( 'r' == $_GET['at'] ){
  $list_type = 'reprint';
} else if ( 'g' == $_GET['at'] ) {
  $list_type = 'general';
} else {
  $list_type = false;
}

?>
<?php get_header(); ?>


  <div class="HolyGrail-body-post" style="margin-top: 0 !important;">
    <main class="HolyGrail-content-post" style="margin-top:0;">
    
    <div class="category-nav" style="max-width: 1019px;">
      <div class="col-lg-12">

        <div class="newest-selection">

        <ul class="nav nav-pills" style="margin-top:20px; font-weight:900;">
          <li<?php echo ($list_type == false)?$active_class:''; ?>><a href="/authors/">全部作者</a></li>
          <li<?php echo ($_GET['at'] == 'g')?$active_class:''; ?>><a href="/authors/?at=g">專欄作者</a></li>
          <li<?php echo ($_GET['at'] == 'r')?$active_class:''; ?>><a href="/authors/?at=r">合作轉載</a></li>
        </ul>
      </div>

      </div>
      <div class="clearfix"></div>

    </div>

    <div class="col-lg-12" style="margin-top:30px;">
<?php

$authors = get_authors_has_post();

if($authors){

  // seperate authors to 2 group: orginal, reprint
  $author_container = array(array(), array());

  foreach($authors as $author_id){

    if( 'admin' == get_the_author_meta( 'user_nicename', $author_id )) continue;

          $archive_url        = get_author_posts_url($author_id);
          $author_name        = get_the_author_meta( 'display_name', $author_id );
          $avatar_html        = get_avatar_url(get_avatar( $author_id, 106 ));
          $author_desc        = get_user_meta($author_id, 'description', true);
          $author_type        = get_user_meta($author_id, 'reprint', true);
          $author_post_count  = count_user_posts($author_id);

          $item = array(
            'id'         => $author_id,
            'name'         => $author_name,
            'avatar'       => $avatar_html,
            'url'          => $archive_url,
            'desc'         => $author_desc,
            'reprint'      => $author_type,
            'post_count'   => $author_post_count,
          );

          if( $list_type !== false and 1 == $author_type ){
            $key = 1;
          } else {
            $key = 0;
          }

          $author_container[$key][] = $item;

  }

  if('reprint' == $list_type){
    $new_authors = $author_container[1];
  } else {
    $new_authors = $author_container[0];
  }

  $reprint_mark = '<span class="label label-default" style="margin: 0px 4px 0px 9px;">合作轉載</span>';

  foreach($new_authors as $author){

    $author_post = get_latest_list_data(1, 1, false, false, $author['id']);
    $latest_post = array_pop($author_post);    

    echo '
        <div class="author-list-div  col-lg-4 col-md-4 col-sm-6 col-xs-12 item">
          <div>
            <div class="post-cover" style=" background-size: 100%; background-image: url(\'' . $latest_post['thumbnail'] . '\'); height:150px; clear: both; border-style: solid;border-width: 1px;border-bottom-width: 0px;border-color: #E9E9E9;">
              <a href="' . $latest_post['permalink'] . '">
              <h4 class="text-center">' . $latest_post['title'] . '</h4>
              </a>
            </div>
            <div class="author-list-box" style="margin-top: -21px;border-style: solid;border-width: 1px;border-top-width: 0px;border-color: #E9E9E9;">
              <img src="' . $author['avatar'] . '" style="width: 60px; height: auto; border-radius: 60px; float: left; margin-right: 9px; margin-left: 23px; border-style: solid; border-width: 2px; border-color: #D3D3D3; color:black;">
              <h5 style="padding-top:20px;margin:5px 0 3px 0;">' . $author['name'] . '</h5>
              <h6 style="color:#837F7F; padding:0; margin:0;">(共 ' . $author['post_count'] . ' 篇) ' . (( 1 == $author['reprint'] )? $reprint_mark : '') . '</h6>
              <p style="color: #8D8D8D;font-size: 13px;margin: 10px 25px 15px 25px;
line-height: 20px;">' . $author['desc'] . '</p><p class="read-more"><a href="' . $author['url'] . '" class="button text-center" style="font-size:14px;"><br><br>Read More</a></p>
            </div>
          
          </div>
        </div>
    ';
                         

  }


}

?>

    </div>



    <div class="clear-fix"></div>

  
    </main>
<?php get_template_part( 'sidebar' ); ?>

  </div>

<?php get_footer(); ?>