<?php get_header(); ?>
<?php

$author_type = get_the_author_meta('reprint');
if( 1 == $author_type ){
  $reprint_mark = '<div class="label label-default" style="margin: 0px 4px 0px 9px;">合作轉載</div>';
} else {
  $reprint_mark = '';
}

$is_sponsor_author = (get_the_author_meta('sponsor_author') == 1);
if($is_sponsor_author){

  $sponsor_id = get_the_author_meta('user_nicename');
  $page_title_height = '230';

  $bg_file = '/sponsor/author/' . $sponsor_id . '.jpg';
  if(is_file(dirname( __FILE__ ) . $bg_file)){
    $page_title_bg = "background-image:url('" . get_template_directory_uri() . $bg_file . "'); background-size:100% auto;";
  } else {
    $page_title_bg = '';
  }
} else {
  $page_title_height = '120';
  $page_title_bg = '';
}

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

$author_id = get_query_var('author');
$html_avatar = get_avatar_url(get_avatar( $post->post_author, 100 ));

$headline_list = get_latest_list_data(1, 1, false, false, $author_id);
$headline = array_pop($headline_list);
$headline_cat = '';



?>
    <div class=" hero-content" style="position: absolute; color: white;  width: 100%;">
      <div style="width:400px; height:10%; background-color:#fff;"></div>
    </div>
    <div class="category-header text-center"  style="verticle-align:middle; height:<?php echo $page_title_height; ?>px; font-size:35px; padding-top:37px;<?php echo $page_title_bg; ?>">
    </div>

  <div class="HolyGrail-body-post tnl-container" style="margin-top: 0;">

    <main class="HolyGrail-content-post" style="margin-top:0;z-index:10;">
       <div class="author-info text-center" style="">
    <img src="<?php echo $html_avatar; ?>" class="" alt="" style="z-index:100;float:center; width:100px; height:100px; padding:4px; background-color:#fff; margin-top:-50px;">
    <div>
      <h1 style="margin:10px; padding: 0;"><?php coauthors(' ', ' '); ?></h1>
<?php if($is_sponsor_author): ?>
      <h5 class="text-center"> <span class="label label-primary " style="float:none;">Sponsor Content</span></h5>
<?php endif; ?>
      <div class="" ><?php echo $reprint_mark; ?><p style="margin-left:0px;"><?php echo str_replace("\n", '<br>', get_the_author_meta( 'description' )); ?></p></div>
      <div class="author-social">
<?php
author_add_social('facebook');
author_add_social('googleplus');
author_add_social('twitter');
author_add_social('linkedin');
author_add_social('youtube');
?>
      </div>
      <div class="author-website">
<?php
                $authorWebsite = get_the_author_meta('url');
                if (!empty($authorWebsite)) {
                    echo '<a href="' . $authorWebsite . '" target="_blank">' . $authorWebsite . '</a>';
                }
?>
      </div>

    </div>
  </div>

      <div class="category-nav" style="">
        <div class="col-lg-12">
          <div class="">
            <h1 style="padding: 15px 31px 10px 0px; color:rgb(117, 117, 117);"><?php coauthors(' ', ' '); ?> 最新的文章</h1>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>

      <div class="category-nav" style="border-bottom: 0px;
      min-height: 8px !important;
      margin-top: -17px;
      margin-bottom: 10;
      color: #ADADAD;">
      <span style="float:right; margin-right:25px;">第 <?php echo $paged; ?> 頁 </span>
      </div>



<?php
$item_count = 10;
$author_id = get_query_var('author');
$post_list = get_latest_list_data($item_count, $paged, false, false, $author_id);

if ( count($post_list) > 0 ) {
  $postCount = 0;
  foreach ( $post_list as $data){
?>
          <div class="post-list-item">
            <img src="<?php echo $data['art_thumb']; ?>" class="media-object" alt="<?php echo $data['title']; ?>" title="<?php echo $data['title']; ?>">
            <?php echo get_video_icon($data['is_video']); ?>
            <div class="post-list-item-content">
              <a href="<?php echo $data['permalink']; ?>">
                <h3><?php echo $data['title']; ?></h3>
                <h6>
                  <span style="float:left;margin-top: 3px;"><a href="<?php echo $data['author_link']; ?>"><?php echo $data['author_name']; ?></a></span>
                  <span style="display:inline-block;">在 <abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr> 發表  • <?php echo $data['social_count']; ?></span> <span class="glyphicon glyphicon-share" style="margin: 2px;"></span>
                  </h6>
              </a>
            </div>
          </div>
<?php
  }
} else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
}

?>
          <div class="pager-div" style="text-align:center;border-top: 1px solid #eee;">
            <?php wpbeginner_numeric_posts_nav(get_latest_list_data($item_count, $paged, false, false, $author_id, true), true); ?>
          </div>
<?php if(! $is_sponsor_author): ?>
          <div class="bs-callout bs-callout-info" style="margin-top: 40px;"><?php require('inc/SF_STORY_BELOW-TITLE_728x90.php'); ?></div>
<?php endif; ?>

  
    </main>

    <aside class="HolyGrail-right hidden-mid tnl-subject-wrap" style="background-color:rgb(230, 230, 230);">
<?php if($is_sponsor_author): ?>
<?php
        $sp1 = dirname( __FILE__ ) . '/sponsor/author/' . $sponsor_id . '1.php';
        if(is_file($sp1)){
            require($sp1);
        }
?>
<?php else: ?>
      <div class="tnl-panel">
        <?php require('inc/SF_STORY_SIDEBAR-HEAD_300x250.php'); ?>
      </div>
<?php endif; ?>
      <?php require('inc/follow_us.php'); ?>
<?php if($is_sponsor_author): ?>
<?php
    $sp2 = dirname( __FILE__ ) . '/sponsor/author/' . $sponsor_id . '2.php';
    if(is_file($sp2)){
        require($sp2);
    }
?>
<?php endif; ?>
      <div class="tnl-panel">
        <?php require('inc/hot_post.php'); ?>
      </div>
    
    </aside>

  </div>

<?php get_footer(); ?>