<?php get_header(); ?>
<!-- InPage 廣告插入點(開始) -->
 <div id="div-onead-ad" style="margin-top:50px; background-color: #000;">
 <script type="text/javascript">
        if (typeof(ONEAD) !== "undefined"){
            ONEAD.cmd = ONEAD.cmd || [];
            ONEAD.cmd.push(function(){
                ONEAD_slot('div-onead-ad');
            });
        }
 </script>
</div>
<!-- InPage 廣告插入點(結束) -->
  <div class="HolyGrail-body-post" style="margin-top:0;">

<?php while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'content', get_post_format() ); ?>
<?php endwhile; ?>
<?php

$terms = get_the_terms( get_the_ID(), 'feature_story' );

if(empty($terms)){
  get_template_part( 'sidebar' );
} else {
  get_template_part( 'sidebar_feature_story' );
}
?>

  </div>
<?php get_footer(); ?>