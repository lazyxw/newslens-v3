<?php get_header(); ?>
<?php

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
$cate_id = get_query_var('cat');
$cat_name = get_category($cate_id)->name;
$cate_link = get_category_link($cate_id);
$active_class = ' class="active"';

if( 'n' == $_GET['st'] ){
  $extra_param = array( 'category__and' => array( CATE_NEWS_ID ));
} else if ( 'r' == $_GET['st'] ) {
  $extra_param = array( 'category__not_in' => array( CATE_NEWS_ID ));
} else {
  $extra_param = false;
}


$is_sponsor_cate = ($cate_id == CATE_VIDEO_ID);

if($cate_id == CATE_VIDEO_ID) {
  $all_title = '全部影片';
  $review_title = '非新聞';
} else {
  $review_title = '評論';
  $all_title = '全部文章';
}

if($is_sponsor_cate){
  $sponsor_id = 'video';

  $page_title_height = '230';
  $page_desc = '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">sponsor oh yeah</div>';
  $bg_file = '/sponsor/cate/' . $sponsor_id . '.jpg';
  if(is_file(dirname( __FILE__ ) . $bg_file)){
    $page_title_bg = "background-image:url('" . get_template_directory_uri() . $bg_file . "'); background-size:100% auto;height:auto;";
  } else {
    $page_title_bg = '';
  }
} else {
  $page_title_height = '120';
  $page_title_bg = '';
  $page_desc = '';
}

?>
    <div class=" hero-content" style="position: absolute; color: white;  width: 100%;">
      <div style="width:400px; height:10%; background-color:#fff;"></div>
    </div>
    <div class="category-header text-center"  style="verticle-align:middle; height:<?php echo $page_title_height; ?>px; font-size:35px; padding-top:37px;<?php echo $page_title_bg; ?>">
<?php

    $page_title = wp_title( '-', false);
    $title = explode('-', $page_title); 
    echo $title[0];
    echo $page_desc;
?>
    </div>

  <div class="HolyGrail-body-post tnl-container" style="margin-top:0 !important;">

    <main class="HolyGrail-content-post" style="margin-top:0;">
       
      <div class="category-nav" style="">
      <div class="col-lg-12">

        <div class="newest-selection">

          <ul class="nav nav-pills" style="margin-top:20px; font-weight:900;">
          <li<?php echo ($extra_param == false)?$active_class:''; ?>><a href="<?php echo $cate_link; ?>"><?php echo $all_title; ?></a></li>
          <li<?php echo ($_GET['st'] == 'n')?$active_class:''; ?>><a href="<?php echo $cate_link; ?>?st=n">新聞</a></li>
          <li<?php echo ($_GET['st'] == 'r')?$active_class:''; ?>><a href="<?php echo $cate_link; ?>?st=r"><?php echo $review_title; ?></a></li>
        </ul>
      </div>

      </div>
      <div class="clearfix"></div>

      </div>

    <div class="category-nav" style="border-bottom: 0px;
    min-height: 8px !important;
    margin-top: -17px;
    margin-bottom: 10;
    color: #ADADAD; width:100%">
    <span style="float:right; margin-right:25px;">第 <?php echo $paged; ?> 頁 </span>
    </div>
<?php
$item_count = 10;


$story_list = get_latest_list_data($item_count, $paged, false, $cate_id, false, false, $extra_param);

if ( count($story_list) > 0 ) {
  $postCount = 0;
  foreach ( $story_list as $data){
    get_post_list_item_html($data, 'r');
  }
} else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
}

?>
          <div class="pager-div" style="text-align:center;border-top: 1px solid #eee;">
            <?php wpbeginner_numeric_posts_nav(get_latest_list_data($item_count, $paged, false, $cate_id, false, true, $extra_param), true); ?>
          </div>

          <div class="bs-callout bs-callout-info" style="margin-top: 40px;">
            <?php require('inc/SF_STORY_BELOW-TITLE_728x90.php'); ?>
          </div>
  
    </main>

    <aside class="HolyGrail-right hidden-mid tnl-subject-wrap" style="background-color:rgb(230, 230, 230);">
<?php if($is_sponsor_cate): ?>
<?php
        $sp1 = dirname( __FILE__ ) . '/sponsor/cate/' . $sponsor_id . '1.php';
        if(is_file($sp1)){
            require($sp1);
        }
?>
<?php else: ?>
      <div class="tnl-panel">
        <?php require('inc/SF_STORY_SIDEBAR-HEAD_300x250.php'); ?>
      </div>
<?php endif; ?>
      <?php require('inc/follow_us.php'); ?>
<?php if($is_sponsor_cate): ?>
<?php
    $sp2 = dirname( __FILE__ ) . '/sponsor/cate/' . $sponsor_id . '2.php';
    if(is_file($sp2)){
        require($sp2);
    }
?>
<?php endif; ?>
      <div class="tnl-panel">
        <?php require('inc/hot_post.php'); ?>
      </div>
    
    </aside>

  </div>

<?php get_footer(); ?>