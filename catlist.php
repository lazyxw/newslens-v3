<?php
/*
    Template Name: category list
*/

$head_cat_list = array(
  '新聞' => array( '/news-list/', array('category__in' => array(CATE_NEWS_ID))),
  '評論' => array( '/review-list/', array('category__not_in' => array(CATE_NEWS_ID))),
  '影音' => array( '/topic/videos/', array('category__in' => array(CATE_VIDEO_ID))),
  '城市' => array( '/tag/city-authors/', array('tag_slug__in' => array('city-authors'))),
  '校園' => array( '/tag/campus-authors/', array('tag_slug__in' => array('campus-authors'))),
  '官方Blog' => array( '/topic/official-blog/', array('category__in' => array(29421))),
);

$cat_list = array('國際', '財經', '政治', '社會', '教育', '職場', '科技', '生活', '名人', '醫療', '網摘');
?>

<?php get_header(); ?>


  <div class="HolyGrail-body-post" style="margin-top: 0 !important;;">
    <main class="HolyGrail-content-post" style="margin-top:0;">
   
    <div class="col-lg-12" style="margin-top:30px;">

<?php
foreach( $head_cat_list as $cat_name => $data){
  $cat_link = $data[0];
  $ext_filter = $data[1];
  $cat_post = get_latest_list_data(1, 1, false, false, false, false, $ext_filter);
  $latest_post = array_pop($cat_post);
?>
      <div class="category-div col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="<?php echo $cat_link; ?>">
        <div class="post-cover" style=" background-size: 100%; background-image: url('<?php echo $latest_post['thumbnail']; ?>'); height:150px; clear: both;">
          <h1 class="text-center" style=""><?php echo $cat_name; ?></h1>
        </div>
      </a>
      </div>
      
<?php
}
?>      

      <div class="clearfix"></div>

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-bottom: 1px solid #eee; float:left; min-height: 30px; margin:25px;"></div>
<?php
foreach( $cat_list as $cat_name){
  $cat_id = get_cat_ID( $cat_name );
  $cat_link = get_category_link($cat_id);
  $cat_post = get_latest_list_data(1, 1, false, $cat_id);
  $latest_post = array_pop($cat_post);
?>
      <div class="category-div col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="<?php echo $cat_link; ?>">
        <div class="post-cover" style=" background-size: 100%; background-image: url('<?php echo $latest_post['thumbnail']; ?>'); height:150px; clear: both;">
          <h1 class="text-center" style=""><?php echo $cat_name; ?></h1>
        </div>
      </a>
      </div>
      
<?php
}
?>      

    </div>

    <div class="clear-fix"></div>
    
    <div style="float:left">
          <div class="bs-callout bs-callout-info" style="margin-top: 40px;
    "><?php require('inc/SF_STORY_BELOW-TITLE_728x90.php'); ?></div>

    </div>
        </main>
<?php get_template_part( 'sidebar' ); ?>

  </div>

<?php get_footer(); ?>