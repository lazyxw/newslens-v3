<?php

$terms = get_the_terms( get_the_ID(), 'feature_story' );

?>
<style type="text/css">
.sidebar_scrol{ position:fixed; top:60px; width:300px;}
.sidebar_none{ display:none; }
.sidebar_scrol_footer{ position:fixed; bottom:181px; width:300px; }
</style>

    <aside class="HolyGrail-left-post hidden-xs" style="background-color:#fff; border-right-style:solid; border-width: 1px; border-color: #E0E0E0; background-color:#E0E0E0;">
    </aside>
    <aside class="HolyGrail-right hidden-mid tnl-subject-wrap" style="background-color:rgb(230, 230, 230);">
        <div id="sidebarheight" style="width:320px;">
          <?php require('inc/feature_story_nav_sidebar.php'); ?>
          <?php require('inc/follow_us.php'); ?>
          <?php if(!empty($GLOBALS['fs_config'][$term_name]['sponsor_banner2'])): ?>
          <?php
                  $sp2 = dirname( __FILE__ ) . '/sponsor/feature_story/' . $GLOBALS['fs_config'][$term_name]['sponsor_banner2'];
                  if(is_file($sp2)){
                      require($sp2);
                  }
          ?>
          <?php else: ?>
                <div class="tnl-panel">
                  <?php require('inc/SF_STORY_SIDEBAR-HEAD_300x250.php'); ?>
                </div>
          <?php endif; ?>
        </div>
        <div id="sidebarheight2" class="sidebar_none" style="width:320px;">
          <?php require('inc/feature_story_nav_sidebar2.php'); ?>
          <?php if(!empty($GLOBALS['fs_config'][$term_name]['sponsor_banner2'])): ?>
          <?php
                  $sp2 = dirname( __FILE__ ) . '/sponsor/feature_story/' . $GLOBALS['fs_config'][$term_name]['sponsor_banner2'];
                  if(is_file($sp2)){
                      require($sp2);
                  }
          ?>
          <?php else: ?>
                <div class="tnl-panel">
                  <?php require('inc/SF_STORY_SIDEBAR-HEAD_300x250.php'); ?>
                </div>
          <?php endif; ?>
        </div>  
    </aside>

<script type="text/javascript">    
jQuery(document).ready(function(){  
    jQuery(window).scroll(function () {
    var scrollVal = jQuery(this).scrollTop(); //抓滾輪目前高度
    var sidebarheight = jQuery('#sidebarheight').height(); //抓右側元素的總高
    var mainheight = jQuery("body").height(); //抓左側元素的總高
    var navheight = jQuery("nav").height(); //抓左側元素的總高
    var footerheight = jQuery("footer").height(); //抓左側元素的總高
    //alert(sidebarheight);
        //判斷IE版本
        var isIE = function(ver){
            var b = document.createElement('b')
            b.innerHTML = '<!--[if IE ' + ver + ']><i></i><![endif]-->'
            return b.getElementsByTagName('i').length === 1
        }
        if(isIE(9)){
        //我不執行JQ
        }else{
            jQuery("span.qScrollTop").text(scrollVal);
            if( mainheight-jQuery(window).height()-navheight-footerheight > 2000 ){
                if(scrollVal > sidebarheight-jQuery(window).height()-navheight+300 ){
                    jQuery("#sidebarheight").addClass("sidebar_none"); //新增一個class
                    jQuery("#sidebarheight2").removeClass("sidebar_none"); //移除一個class
                    jQuery("#sidebarheight2").addClass("sidebar_scrol"); //新增一個class
                }
                if(scrollVal < sidebarheight-jQuery(window).height()-navheight+300 ){
                    jQuery("#sidebarheight").removeClass("sidebar_none"); //新增一個class
                    jQuery("#sidebarheight2").removeClass("sidebar_scrol"); //移除一個class
                    jQuery("#sidebarheight2").addClass("sidebar_none"); //新增一個class
                }
                if(scrollVal - 50 > mainheight- jQuery(window).height() - navheight - footerheight ){
                    jQuery("#sidebarheight2").removeClass("sidebar_scrol"); //移除一個class
                    jQuery("#sidebarheight2").addClass("sidebar_scrol_footer"); //新增一個class
                }
                if(scrollVal > sidebarheight -jQuery(window).height()-navheight ){
                    if(scrollVal < mainheight-jQuery(window).height()-navheight-footerheight ){
                        jQuery("#sidebarheight2").removeClass("sidebar_scrol_footer"); //移除一個class
                        jQuery("#sidebarheight2").addClass("sidebar_scrol"); //新增一個classs
                    }
                }
            }
        }
        
    });
});  
</script>  
