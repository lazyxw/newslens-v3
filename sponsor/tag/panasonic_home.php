        <div class="tnl-panel" style="width:97%; margin:1.5%; min-height:200px;">
            <div>
                <a href="/tag/pm2.5">
                    <h3 class="text-center" style="margin:15px 25px 5px 25px;">PM 2.5 空氣中隱形的健康殺手</h3>
                </a>
                <div style="margin:0px 25px 15px 25px; diplay:inline;">
                    你一天要呼吸幾次？你知道你很有可能吸入空氣中隱形的健康殺手？天空灰灰的，你可能以為是「霧」，其實是「霾」！PM2.5已經成為全球高度關注的新興污染物。它是空氣中極微小的懸浮粒子，直徑僅有2.5微米。而這些微小的粒子負載了重金屬、戴奧辛和病菌，如果被人體吸入... <a href="/tag/pm2.5">繼續閱讀</a>
                </div>
            </div>
            <div>
<?php
    $sponsor_filter = array(
        'orderby' => 'rand',
    );

    $sponson_list = get_latest_list_data(2, 1, 'pm2-5', false, 7256, false, $sponsor_filter);

    if ( count($sponson_list) > 0 ) {
      $postCount = 0;
      foreach ( $sponson_list as $data){
?>
                <div class="post-list-item col-lg-6">
                    <a href="<?php echo $data['permalink']; ?>">
                      <img src="<?php echo $data['art_thumb']; ?>" class="media-object" width="66" height="66" scale="2">
                    </a>
                    <div class="post-list-item-content">
                      <a href="<?php echo $data['permalink']; ?>">
                        <h3><?php echo $data['title']; ?></h3>
                        <h6><abbr></abbr></h6>
                      </a>
                    </div>
                </div>
<?php
      }
    }
?>
            </div>
            <div style="float:right; color:#8B8B8B; margin-right:7px; margin-top:-27px;">
                議題贊助：<a href="http://pmst.panasonic.com.tw/active/nanoe/PSPCpm.aspx?nanoe=nanoepm25" target="_blank"><img src="http://www.thenewslens.com/wp-content/uploads/2014/05/download-2.jpeg" style="width:150px; height:auto; opacity:0.6;"></a>
            </div>
        </div>
