<?php
/*
    Template Name: review list
*/
?>

<?php
$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

$headline_list = get_latest_list_data(1, 1, false, '-' . CATE_NEWS_ID);
$headline = array_pop($headline_list);
$headline_cat = '評論';

?>
<?php get_header(); ?>
  <div class="HolyGrail-body-post" style="margin-top:0 !important;">

    <main class="HolyGrail-content-post" style="margin-top:0;">
       
      <div class="category-nav" style="">
        <div class="col-lg-12">
          <div class="newest-title">
            <h1 style="padding: 15px 31px 10px 0px; max-width:250px; color:rgb(117, 117, 117);">最新評論</h1>
          </div>

        </div>
        <div class="clearfix"></div>
      </div>

      <div class="category-nav" style="border-bottom: 0px;
      min-height: 8px !important;
      margin-top: -17px;
      margin-bottom: 10;
      color: #ADADAD;">
        <span style="float:right; margin-right:25px;">第 <?php echo $paged; ?> 頁 </span>
      </div>
<?php
$item_count = 10;
$review_list = get_latest_list_data($item_count, $paged, false, '-' . CATE_NEWS_ID);

if ( count($review_list) > 0 ) {
  $postCount = 0;
  foreach ( $review_list as $data){
    get_post_list_item_html($data, 'r');
  }
} else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
}

?>
          <div class="pager-div" style="text-align:center;border-top: 1px solid #eee;">
            <?php wpbeginner_numeric_posts_nav(get_latest_list_data($item_count, $paged, false, '-' . CATE_NEWS_ID, false, true), true); ?>
          </div>
          <div class="bs-callout bs-callout-info" style="margin-top: 40px;
"><?php require('inc/SF_STORY_BELOW-TITLE_728x90.php'); ?></div>


  
    </main>
<?php get_template_part( 'sidebar' ); ?>

  </div>

<?php get_footer(); ?>