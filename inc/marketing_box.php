<?php
$box_subject = get_option('marketing_block_subject');
$box_link = get_option('marketing_block_link');
$box_image = get_option('marketing_block_image');

if( empty($box_subject) or empty($box_link) or empty($box_image) ) return;


?>

    <!--Marketing Box Here-->
        <div class="tnl-panel" style="width:302px;" >
        <div style="border-bottom:1px solid rgb(230, 230, 230); color:#A8A8A8;"><img src="http://www.thenewslens.com/wp-content/themes/newslens_v3/img/logo.png" style="width:200px; height:auto; margin:10px 15px; "></div>
          <a href="<?php echo $box_link; ?>"><img src="<?php echo $box_image; ?>" style="width:300px; height:auto;">
          <button type="button" class="btn btn-success" style="width: 90%; margin: 1% 5% 4% 5%;"><?php echo $box_subject; ?></button></a>
           <hr style="margin-top: 10px; margin-bottom: 0px;">
<?php
    $marketing_list = get_latest_list_data(1, 1, '關鍵那一年');

    if ( count($marketing_list) > 0 ) {
      foreach ( $marketing_list as $data){
?>
          <!--post here-->
          <div class="post-list-item">
            <img src="<?php echo $data['art_thumb']; ?>" class="media-object" alt="<?php echo $data['title']; ?>" title="<?php echo $data['title']; ?>">
            <?php echo get_video_icon($data['is_video']); ?>
            <div class="post-list-item-content">
              <a href="<?php echo $data['permalink']; ?>">
                <h3><?php echo $data['title']; ?></h3>
                <h6><abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr></h6>
              </a>
            </div>
          </div>    <!--End of post-->  
<?php
      }
    }
?>
        </div>    <!--End of Marketing-->        
        <hr style="margin:15px;">
