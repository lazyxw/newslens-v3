        <div class="tnl-subject">
          <span class="glyphicon glyphicon-play-circle" style="margin: 2px; float: left;"></span><span>影音新聞 Video News</span>
          <hr>
          <a href="<?php echo get_category_link( CATE_VIDEO_ID ); ?>">
          <span class="more-content">更多</span>
          </a>
        </div>

<?php
    $video_news_list = get_latest_list_data(2, 1, false, false, false, false, array( 'category__and' => array( CATE_NEWS_ID, CATE_VIDEO_ID ) ));

    if ( count($video_news_list) > 0 ) {
      $postCount = 0;
      foreach ( $video_news_list as $data){
?>
          <div class="post-list-item">
            <img src="<?php echo $data['art_thumb']; ?>" class="media-object" alt="<?php echo $data['title']; ?>" title="<?php echo $data['title']; ?>">
            <div class="post-list-item-content">
              <a href="<?php echo $data['permalink']; ?>">
                <h3><?php echo $data['title']; ?></h3>
                <h6><abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr></h6>
              </a>
            </div>
          </div>
<?php
      }
    } else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
    }
