<?php

$terms = get_the_terms( get_the_ID(), 'feature_story' );

if(empty($terms)) return;

$this_term = array_pop($terms);
$term_name =$this_term->name;
$this_term_url = get_term_link($this_term);
$this_term_bg = get_photon_url($GLOBALS['fs_config'][$term_name]['bg_img']);

$extra_filter = array(
  'feature_story' => $this_term->slug,
  'order' => 'ASC',
);

$fs_list = get_latest_list_data(-1, 1, false, false, false, false, $extra_filter);

if(empty($fs_list)) return;

?>
    <div class="tnl-panel listArticle topicContent">
      <div>
        <div class="tnl-subject"><?php echo $term_name; ?>
          <hr>
        </div>
        <?php foreach($fs_list as $i => $data): ?>
        <div class="post-list-item-hot<?php echo ($data['post_id'] == get_the_id())?' active':''; ?>">
          <div class="img media-object" style="background-image:url(<?php echo $data['thumbnail']; ?>)">
            <h1><?php echo $i+1; ?></h1>
          </div>
          <div class="post-list-item-content-hot"> <a href="<?php echo $data['permalink']; ?>">
            <h3><?php echo $data['title']; ?></h3>
            </a> </div>
        </div>
        <?php endforeach; ?>
      </div>
    </div>
