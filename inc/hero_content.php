<?php
$hc_id = get_option('hero_content_post_id');
$hc_post = get_post($hc_id);

if( empty($hc_id) or is_null($hc_post) ) {
    $head_story_list = get_latest_list_data(1);
}else{
    $author_id = $hc_post->post_author;
    $timestamp = strtotime(get_gmt_from_date($hc_post->post_date));
    $featured_img_id = get_post_thumbnail_id($hc_id);
    $featured_img_obj = get_post($featured_img_id);
    $photo_credit = strip_tags($featured_img_obj->post_excerpt);
    $full_image_url = get_photon_url($featured_img_obj->guid);

    $head_story_list[] = array(
        'permalink'             => get_permalink($hc_id),
        'title'                 => $hc_post->post_title,
        'timestamp'             => $timestamp,
        'photo_credit'          => $photo_credit,
        'full_featured_img'     => $full_image_url,
        'author_id'             => $author_id,
        'author_link'           => get_author_posts_url(get_the_author_meta( 'ID', $author_id  )),
        'author_name'           => get_the_author_meta( 'display_name', $author_id  ),
    );
}

$data = array_pop($head_story_list);
$html_avatar = get_photon_url(get_avatar_url(get_avatar( $data['author_id'], 100 )));

?>
  <div id="hero" style="background-image: url('<?php echo $data['full_featured_img']; ?>');">
    <div class=" hero-content" style="">
        <div class="">
        <a href="<?php echo $data['permalink']; ?>">
        <div class="text-center">
            <h3 class="" style=" color:#fff; font-weight:900;"><?php echo $data['title']; ?></h3>
            <div style="width: 300px;margin: 0 auto; margin-top:17px;">
                <h6 style="color:#C5C5C5; font-size:15px;"><img src="<?php echo $html_avatar; ?>" class="" alt="" style="width: 37px; height: auto; border-radius: 37px; margin-right: 9px; border-style: solid; border-width: 2px; border-color: #D3D3D3;"><a href="<?php echo $data['author_link']; ?>"><?php echo $data['author_name']; ?></a> • <abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr></h6>
            </div>
        </div>
        </a>
        </div>
    </div>
  </div>
  