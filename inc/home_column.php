<?php

if(!empty($GLOBALS['fs_config'])){
?>

<div style="background-color: rgb(240, 240, 240);overflow:auto;" class="tnl-subject-wrap">
<div class="tnl-panel" style="width:97%; margin:1.5%; min-height:200px;overflow:auto;">
  <div class="tnl-subject">
      <span>特別報導 Special Report</span>
      <hr>
  </div>
<?php
  
  $rs_count = 4;

  $i = 0;

  foreach($GLOBALS['fs_config'] as $key => $value){
    $this_term_name = $key;
    $this_term = get_term_by('name', $this_term_name, 'feature_story');
    if(empty($this_term)) continue;

    $this_term_url = get_term_link($this_term);
    $this_term_bg = get_photon_url($value['bg_img']);
    $this_term_desc = summarize_long_text($this_term->description, 70) . '...';
?>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="margin-bottom:15px; margin-top: 15px;">
          <div>
            <div class="post-cover" style="position:relative;background-size: 100%; background-image: url(<?php echo $this_term_bg; ?>); height:150px; clear: both;border-width: 0px;"></div>
            <div class="author-list-box" style="margin-top: -21px;border-width: 0px;">
              <a href="<?php echo $this_term_url; ?>"><br>
              <h4 class="text-center" style="line-height: 24px;margin-top: 10px;height:25px;overflow: hidden;"><?php echo $this_term_name; ?></h4>
              <div style="color: #8D8D8D;font-size: 13px;margin: 0 25px 15px 25px;line-height: 20px;height:100px;overflow: hidden;"><?php echo $this_term_desc; ?></div>
              </a>
              <?php if(!empty($value['sponsor_text'])): ?>
              <div style="position:absolute; bottom: 5px;right:5px; color:#8B8B8B; ">議題贊助：<?php echo $value['sponsor_text']; ?></div>
              <?php endif; ?>
            </div>
          </div>
        </div>
<?php
    $i++;

    if($i >= $rs_count) break;
  }
?>
</div>
</div>
<?php
}
?>