  <div class="HolyGrail-body tnl-subject-wrap">
    <main class="HolyGrail-content">
        <div class="">
            <div class="tnl-subject">
                <span>最新新聞 Newest Headlines</span>
                <hr>
                <a href="/news-list/">
                <span class="more-content">更多新聞</span>
                </a>
            </div>
<?php
    $news_list = get_latest_list_data(6, 1, false, CATE_NEWS_ID);

    if ( count($news_list) > 0 ) {
      $postCount = 0;
      foreach ( $news_list as $data){
        get_post_list_item_html($data, 'l', true);
      }
    } else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
    }
?>
        </div>
       

    </main>
    <aside class="HolyGrail-left hidden-xs" style="border-right-style:solid; border-width: 1px; border-color: #E0E0E0;">

        <div class="" style="background-color:#fff;">
            <div class="tnl-subject">
                <span>熱門新聞 Trending News</span>
                <hr>
            </div>

<?php
    $hot_news_filter = array(
        'meta_key' => 'social_total_count',
        'orderby' => 'meta_value_num',
        'date_query' => array(
            array(
                'after'  => '3 days ago',
            )
        )
    );

    $hot_news_list_count = 5;
    $hot_news_list = get_latest_list_data($hot_news_list_count, 1, false, CATE_NEWS_ID, false, false, $hot_news_filter);
/*
    // sponsor content
    $sponsor_story = get_latest_list_data(1, 1, 'panasonic-viera');
    array_splice($hot_news_list, 0, 0, $sponsor_story);
    if( count($hot_news_list) > $hot_news_list_count ){
      array_pop($hot_news_list);
    }
*/

    if ( count($hot_news_list) > 0 ) {
      $postCount = 0;
      foreach ( $hot_news_list as $data){
        get_post_list_item_html($data, 'h');
      }
    } else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
    }
?>
        <div class="tnl-panel" style="width:302px;">
          <iframe src="http://www.tripadvisor.com/WidgetEmbed-tcphoto?geos=2&placeTypes=geo,attractions&partnerId=ADBA12AFCFD84F4E86287107BC4A7A9A&lang=zh_TW&display=true" width="300" height="250" frameborder="0" style="" scrolling="no">
        </iframe> 
        
        </div>
        </div>


        
          </aside>
    <aside class="HolyGrail-right hidden-mid" style="background-color:rgb(240, 240, 240);border-left-style: solid; border-width: 1px; border-color: #E0E0E0;">
        <?php require('marketing_box.php'); ?>
            <div class="tnl-panel" style="width:302px;">
        <?php require('SF_HOME_POPULAR-1ST_300x250.php'); ?>
          </div>
        <?php require('follow_us.php'); ?>
    </aside>
  </div>
  <div style="background-color: rgb(240, 240, 240);" class="tnl-subject-wrap">
    <?php require( dirname( __FILE__ ) . '/../sponsor/tag/panasonic_home.php'); ?>
  </div>
