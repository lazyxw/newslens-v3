<?php if( is_singular() ): ?>
<!-- OneAD InRead 與 InPage 開始 -->
<script type="text/javascript">
    var ONEAD = {};
        ONEAD.channel =  21; // 關鍵評論網
        ONEAD.volume =  0.2; // range is 0 to 1 (float)    
        ONEAD.slot_limit = {width: 990, height: 460};
        ONEAD.category = '-1';
         // optional(s)
        ONEAD.slot_limit_multiple = {
            inread: {
                width: 640,
                height: 390
            }
        };
        ONEAD.response_freq = {start:1, step: 3};
        ONEAD.response_freq_multiple = {
            instream: "1,4,7,10,13,16,19",
            inread: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20"
        };
    ONEAD.cmd = ONEAD.cmd || [];
</script>
<script type="text/javascript">
    // For OneAD, DON'T MODIFY the following
    if (typeof(ONEAD) !== "undefined"){

        ONEAD.uid = "1000046";    // 關鍵評論網正式帳號

        ONEAD.external_url = "http://onead.onevision.com.tw/"; // OneAD 影音平台正式平台
        ONEAD.wrapper = 'ONEAD_player_wrapper';
        ONEAD.wrapper_multiple = {
            instream: "ONEAD_player_wrapper", // equals to inpage
            inread: "ONEAD_inread_wrapper",
            incover: "ONEAD_incover_wrapper"
        };
    }
    if (typeof window.isip_js == "undefined") {
        (function() {

        var src = 'http://ad-specs.guoshipartners.com/static/js/isip.v2.js'; // OneAD 影音平台正式平台
         var js = document.createElement('script');
        js.async = true;
        js.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        js.src = src;
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(js, node.nextSibling); // insert after
        })();
    }
</script>
<script type="text/javascript">
    ONEAD_on_get_response = function(param){
    // if there is no pid, param is {}, it's not null
    if (param === null || param.pid === undefined){
        // 沒廣告
    }else{
        var t = setInterval(function(){
            if (ONEAD_is_above(100)){    }
                 }, 1000);
             }
         }
        // 這個函式名稱是固定的，廣告播放完畢會呼叫之
        function changeADState(obj){
            if (obj.newstate == 'COMPLETED' || obj.newstate == 'DELETED' ){
                if (ONEAD.play_mode == 'incover'){
                    // remove the dimming block
                    ONEAD_cleanup(ONEAD.play_mode);
                }else{
                    ONEAD_cleanup();
                }
            }
        }
</script>
<!-- OneAD InRead 與 InPage 結束 -->

<?php endif; ?>
