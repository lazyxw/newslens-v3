<?php
$topic_array = explode(',', get_option('hero_content_tag'));

$i = 0;
if( ! empty($topic_array[0]) ){
?>
  <div style="background-color: rgba(0, 0, 0, 0.65);padding: 13px;font-weight: 900;color: rgb(228, 228, 228);margin-top: -50px;" class="tnl-subject-wrap text-center">
    <span class="glyphicon glyphicon-globe" style="padding-top:2px"></span>
    <span>熱門議題： 
<?php foreach($topic_array as $key => $data): ?>
<?php echo ($key == 0)?'':'、'; ?>
      <a href="/tag/<?php echo trim($data); ?>/" style="color:#fff;"><?php echo trim($data); ?></a>
<?php endforeach; ?>
  </div>

<?
}
?>