<?php
$min_posts = 1; // Make sure it's int, it's not escaped in the query

$author_ids = $wpdb->get_col("SELECT `post_author` FROM
    (SELECT `post_author`, COUNT(*) AS `count` FROM {$wpdb->posts}
        WHERE `post_type`= 'post' and `post_status`='publish' GROUP BY `post_author`) AS `stats`
    WHERE `count` >= 3 ORDER BY `count` DESC, post_author limit 20;");
// Do what you want to $author_ids from here on...

// print_r($author_ids);
// return;

$target_authors = array_rand($author_ids, 2);

foreach( $target_authors as $author_id ) {
  $target_author_id = $author_ids[$author_id];

  $archive_url        = get_author_posts_url($target_author_id);
  $author_name        = get_the_author_meta( 'display_name', $target_author_id );
  $avatar             = get_avatar_url(get_avatar( $target_author_id, 64 ));
  $author_type        = get_user_meta($author_id, 'reprint', true);
  $author_post_count  = count_user_posts($author_id);

?>
      <div class="tnl-panel">
        <div class="tnl-subject" style="padding-bottom:0;">
          
          <div class="pull-left">
            <span>本週熱門作者: <span style="display: inline-block; float:right;"><a href="<?php echo $archive_url; ?>"><?php echo $author_name; ?></span></span>
          </div>
          <div class="pull-right">
            <img src="<?php echo $avatar; ?>" class="pull-right" alt="" style="width:40px; height:auto; border-radius:40px;">
          </div>
        </div>
        <hr style="margin-bottom:0;">

<?php
    $featured_author_list = get_latest_list_data(3, 1, false, false, $target_author_id );

    if ( count($featured_author_list) > 0 ) {
      $postCount = 0;
      foreach ( $featured_author_list as $data){
?>
        <div class="post-list-item">
          <img src="<?php echo $data['art_thumb']; ?>" class="media-object" alt="<?php echo $data['title']; ?>" title="<?php echo $data['title']; ?>">
          <?php echo get_video_icon($data['is_video']); ?>
          <div class="post-list-item-content">
            <a href="<?php echo $data['permalink']; ?>">
              <h3><?php echo $data['title']; ?></h3>
              <h6><abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr></h6>
            </a>
          </div>
        </div>
<?php
      }
    } else {
?>
      <div class="post-list-item">
        <h3>沒有相關文章</h3>
      </div>
<?php
    }
?>
        <div class="tnl-subject">
          <a href="<?php echo $archive_url; ?>"><span class="more-content-bottom" style="padding-top:15px;">更多文章</span></a>
          <hr style="margin-top: 5px;">
        </div>
      </div>
<?php
}

