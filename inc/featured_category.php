<?php
$args = array(
  'number' => 10,
  'orderby' => 'count'
);

$cates = get_categories( $args );

$obj_cat = $cates[array_rand($cates)];
$cat_link = get_category_link( $obj_cat->term_id );
?>
        <!--This is the News Section-->
      <div class="tnl-subject">
        <span>精選分類 Featured Category: <a href="<?php echo $cat_link; ?>"><span class="label label-warning" style="background-color: #f0ad4e;margin: 0px 4px 0px 9px;"><?php echo $obj_cat->cat_name; ?></span></a></span>
      </div>
<?php
    $featured_cat_list = get_latest_list_data(3, 1, false, $obj_cat->term_id );

    if ( count($featured_cat_list) > 0 ) {
      $postCount = 0;
      foreach ( $featured_cat_list as $data){
?>
        <div class="post-list-item">
          <img src="<?php echo $data['art_thumb']; ?>" class="media-object" alt="<?php echo $data['title']; ?>" title="<?php echo $data['title']; ?>">
          <div class="post-list-item-content">
            <a href="<?php echo $data['permalink']; ?>">
              <h3><?php echo $data['title']; ?></h3>
              <h6><abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr></h6>
            </a>
          </div>
        </div>
<?php
      }
    } else {
?>
      <div class="post-list-item">
        <h3>沒有相關文章</h3>
      </div>
<?php
    }
?>
        <img src="<?php echo get_template_directory_uri(); ?>/img/nvesto.png" class="media-object" alt="" style="padding:11px; width:320px; height:auto;">
        
        <div class="tnl-subject">
          <a href="<?php echo $cat_link; ?>"><span class="more-content-bottom" style="padding-top:15px;">更多<?php echo $obj_cat->cat_name; ?>內容</span></a>
          <hr style="margin-top: 5px;">
        </div> 
