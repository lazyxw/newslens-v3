<?php
$head_story_list = get_latest_list_data(1);
$data = array_pop($head_story_list);

define('HEADLINE_BG', $data['full_featured_img']);
?>
        <div class="post-list-item">
          <a href="<?php echo $data['permalink']; ?>">
          <div class="text-center">
            <h3 class="" style=" color:#fff; font-weight:900;"><?php echo $data['title']; ?></h3>
            <div style="width: 300px;margin: 0 auto; margin-top:17px;">
              <h6 style="color:#C5C5C5; font-size:15px;"><img src="<?php echo $data['thumbnail']; ?>" class="" alt="" style="width: 37px; height: auto; border-radius: 37px; margin-right: 9px; border-style: solid; border-width: 2px; border-color: #D3D3D3;"><abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr> • <?php echo $data['social_count']; ?><span class="glyphicon glyphicon-share" style="margin: 2px;"></span></h6>
            </div>
          </div>
          </a>
        </div>
