<?php
$args = array(
  'number' => 100,
  'orderby' => 'count'
);

$tags = get_tags( $args );

$obj_tag = $tags[array_rand($tags)];
$tag_link = get_tag_link( $obj_tag->term_id );
?>

        <!--This is the News Section-->
        <div class="tnl-subject">
          <span>精選話題 Featured Topic: <a href="<?php echo $tag_link; ?>"><span class="label label-warning" style="background-color: #0FAFC5; margin: 0px 4px 0px 9px;"><?php echo $obj_tag->name; ?></span></span>
        </div>

<?php
    $featured_tag_list = get_latest_list_data(3, 1, $obj_tag->slug );

    if ( count($featured_tag_list) > 0 ) {
      $postCount = 0;
      foreach ( $featured_tag_list as $data){
?>
        <div class="post-list-item">
          <img src="<?php echo $data['art_thumb']; ?>" class="media-object" alt="<?php echo $data['title']; ?>" title="<?php echo $data['title']; ?>">
          <div class="post-list-item-content">
            <a href="<?php echo $data['permalink']; ?>">
              <h3><?php echo $data['title']; ?></h3>
              <h6><abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr></h6>
            </a>
          </div>
        </div>
<?php
      }
    } else {
?>
      <div class="post-list-item">
        <h3>沒有相關文章</h3>
      </div>
<?php
    }
?>
        <div class="tnl-subject">
          <a href="<?php echo $tag_link; ?>"><span class="more-content-bottom" style="padding-top:15px;">更多關於<?php echo $obj_tag->name; ?>的內容</span></a>
          <hr style="margin-top: 5px;">
        </div> 
