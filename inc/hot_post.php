        <div style="height: 570px;"><!--This is the Trending Opinion Section-->
          <div class="tnl-subject">
            <span>熱門文章 Trending Articles</span>
            <hr>
          </div>
<?php
    $hot_list = get_popular_list_with_GA(5);

    if ( count($hot_list) > 0 ) {
      $postCount = 0;
      foreach ( $hot_list as $data){
?>
          <div class="post-list-item-hot">
            <img src="<?php echo $data['art_thumb']; ?>" class="media-object" alt="<?php echo $data['title']; ?>" title="<?php echo $data['title']; ?>">
            <div class="post-list-item-content-hot">
              <a href="<?php echo $data['permalink']; ?>?ref=hotpost">
                <h3><?php echo $data['title']; ?></h3>
                <h6><abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr> • <?php echo $data['social_count']; ?><span class="glyphicon glyphicon-share" style="margin: 2px;"></span></h6>
              </a>
            </div>
          </div>
<?php
      }
    } else {
?>
        <div class="post-list-item-hot">
          <h3>沒有相關文章</h3>
        </div>
<?php
    }
?>
        </div>
