<?php

$block_title = '最新回應';
$comment_length = 40;
$comments = get_recent_comments();

if ( count($comments) > 0 ) {

?>
      <div class="tnl-panel">
        <!--This is the News Section-->
        <div class="tnl-subject" style="padding-bottom:0;">
          <span>熱門回應 Trending Discussion</span><span class="glyphicon glyphicon-comment" style="margin: 2px;"></span>
        </div>
        <hr style="margin-bottom:0;">
<?php
  foreach ( $comments as $obj ) {
      $post_id = $obj->comment_post_ID;
      $post = get_post( $post_id );
      $timestamp = strtotime(get_gmt_from_date($obj->comment_date_gmt));
      $post_url = get_permalink( $post_id );
      $comment_url = $post_url . '?ref=rc#comment-' . $obj->meta_value;
      $summary = $obj->comment_author . ': ' . summarize_long_text ( $obj->comment_content, $comment_length ) . ((mb_strlen( $obj->comment_content ) > $comment_length)?' ...':'');
?>
        <div class="tnl-hot-comment">
        <strong><?php echo $obj->comment_author; ?></strong> 在 <strong><a href="<?php echo $post_url; ?>"><?php echo $post->post_title; ?></a></strong> 中提到：<a href="<?php echo $comment_url; ?>"><?php echo $summary; ?></a>
        </div>
<?

  }

?>
      </div>
<?php
}
