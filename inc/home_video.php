   <div class="HolyGrail-body tnl-subject-wrap">
    <main class="HolyGrail-content">
      <div class="">
        <div class="tnl-subject">
          <span>最新影音內容 Newest Videos</span>
          <hr>
          <a href="/topic/videos/">
          <span class="more-content">更多影音</span>
          </a>
        </div>
<?php
    $video_list = get_latest_list_data(4, 1, false, CATE_VIDEO_ID);

    if ( count($video_list) > 0 ) {
      $postCount = 0;
      foreach ( $video_list as $data){
        get_post_list_item_html($data, 'l');
      }
    } else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
    }
?>
      </div>
     

    </main>
    <aside class="HolyGrail-left hidden-xs" style=" border-right-style:solid; border-width: 1px; border-color: #E0E0E0;">

        <div class="" style="background-color:#fff;">
        <div class="tnl-subject">
          </span><span>熱門影音內容 Trending Videos</span>
          <hr>
        </div>
<?php
    $hot_video_filter = array(
        'meta_key' => 'social_total_count',
        'orderby' => 'meta_value_num',
        'date_query' => array(
            array(
                'after'  => '3 days ago',
            )
        )
    );

    $hot_video_list = get_latest_list_data(4, 1, false, CATE_VIDEO_ID, false, false, $hot_video_filter);

    if ( count($hot_video_list) > 0 ) {
      $postCount = 0;
      foreach ( $hot_video_list as $data){
        get_post_list_item_html($data, 'h');
      }
    } else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
    }
?>
        </div>

   
    



    </aside>
    <aside class="HolyGrail-right hidden-mid" style="border-left-style: solid; border-width: 1px; border-color: #E0E0E0;">

<div>
  <div class="tnl-subject">
          <span>產業資訊</span>
          <hr>
  </div>
  <div class="post-list-item">
    <a href="http://www.thenewslens.com/sponsor-content4/">
      <img src="http://i2.wp.com/www.thenewslens.com/wp-content/uploads/2014/08/04_%E6%B4%BB%E5%8B%95%E6%B5%B7%E5%A0%B1_2014E+%E7%AC%AC%E4%BA%94%E5%B1%86%E5%9C%8B%E9%9A%9B%E5%89%B5%E6%A5%AD%E8%82%B2%E6%88%90%E8%AB%96%E5%A3%87.png?zoom=2&resize=720%2C1019?zoom=2&amp;resize=66%2C66" class="media-object" alt="" title="" width="66" height="66" src-orig="" scale="2">
    </a>
    <div class="post-list-item-content">
      <a href="http://www.thenewslens.com/sponsor-content4/">
      <h3>人乘N次方的補帖祕方 最跨領域的熱血告白</h3>
        <h6><abbr></abbr></h6>
      </a>
    </div>
   </div>
  <div class="post-list-item">
    <a href="http://www.thenewslens.com/sponsor-content3/">
        <img src="http://www.thenewslens.com/wp-content/uploads/2014/05/PUMA台灣區總經理於保羅、林可彤及林義傑自信展示Mobium新品，亮麗起跑圖由PUMA提供.jpg?zoom=2&amp;resize=66%2C66" class="media-object" alt="" title="" width="66" height="66" src-orig="" scale="2">
    </a>
    <div class="post-list-item-content">
      <a href="http://www.thenewslens.com/sponsor-content3/">
        <h3>PUMA玩很大！ 首度挑戰全台巨型懸吊塗鴉 12層樓高空締造傳奇無極限</h3>
        <h6><abbr></abbr></h6>
      </a>
    </div>
   </div>
</div>
<div>
<?php require('SF_HOME_LATEST-BOTTOM_300x250.php'); ?>
</div>
    </aside>
  </div>
