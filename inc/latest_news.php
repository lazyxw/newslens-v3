        <div class="tnl-subject">
          <span>今日新聞 News Headlines</span>
          <hr>
          <a href="/news-list/">
          <span class="more-content">更多新聞</span>
          </a>
        </div>
<?php
    $news_list = get_latest_list_data(2, 1, false, CATE_NEWS_ID);

    if ( count($news_list) > 0 ) {
      $postCount = 0;
      foreach ( $news_list as $data){
?>
          <div class="post-list-item">
            <img src="<?php echo $data['art_thumb']; ?>" class="media-object" alt="<?php echo $data['title']; ?>" title="<?php echo $data['title']; ?>">
            <div class="post-list-item-content">
              <a href="<?php echo $data['permalink']; ?>">
                <h3><?php echo $data['title']; ?></h3>
                <h6><abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr></h6>
              </a>
            </div>
          </div>
<?php
      }
    } else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
    }
