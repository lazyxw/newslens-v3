        <div>
          <div class="tnl-subject">
            <span>最新評論 New Opinions</span>
            <hr>
          </div>
<?php
    $latest_list = get_latest_list_data(10, 1, false, '-' . CATE_NEWS_ID . ',-' . CATE_VIDEO_ID);

    if ( count($latest_list) > 0 ) {
      $postCount = 0;
      foreach ( $latest_list as $data){
?>
          <div class="post-list-item">
            <img src="<?php echo $data['art_thumb']; ?>" class="media-object" alt="<?php echo $data['title']; ?>" title="<?php echo $data['title']; ?>">
            <div class="post-list-item-content">
              <a href="<?php echo $data['permalink']; ?>">
                <h3><?php echo $data['title']; ?></h3>
                <h6><abbr class="timeago" title="<?php echo date('c', $data['timestamp']); ?>"><?php echo date('Y/m/d', $data['timestamp']); ?></abbr></h6>
              </a>
            </div>
          </div>
<?php
    

        $postCount++;

        // if(1 == $postCount ){
        //     if( is_home() ){
        //         require('SF_HOME_LATEST-1ST_300x250.php');
        //     }else{
        //         require('SF_CHANNEL_LATEST-1ST_300x250.php');
        //     }
        // } else if ($posts_per_page == $postCount){
        //     if( is_home() ){
        //         require('SF_HOME_LATEST-BOTTOM_300x250.php');
        //     }else{
        //         require('SF_CHANNEL_LATEST-BOTTOM_300x250.php');
        //     }
        // }
      }
    } else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
    }

?>
          <div class="tnl-subject">
          <a href="/review-list/">
            <span class="more-content-bottom">更多評論</span>
          </a>
            <hr>
          </div>              
        </div>
