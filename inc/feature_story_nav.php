<?php

$terms = get_the_terms( get_the_ID(), 'feature_story' );

if(empty($terms)) return;

$this_term = array_pop($terms);
$term_name =$this_term->name;
$this_term_url = get_term_link($this_term);
$this_term_bg = get_photon_url($GLOBALS['fs_config'][$term_name]['bg_img']);

$previous_post_obj = get_previous_post( true, '', 'feature_story' );
$previous_post_link = get_permalink($previous_post_obj);
$previous_post_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($previous_post_obj->ID));

$next_post_obj = get_next_post( true, '', 'feature_story' );
$next_post_link = get_permalink($next_post_obj);
$next_post_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($next_post_obj->ID));

?>
<div class="topicContent">
  <ul class="listArticle">
    <?php if(!empty($previous_post_obj)): ?>
    <li class="col-md-4"> <a href="<?php echo $previous_post_link; ?>">
      <div class="img" style="background-image: url(<?php echo get_photon_url($previous_post_image_url[0]); ?>)">
        <h4>上篇文章 </h4>
      </div>
      <div class="articleMainContent"><?php echo $previous_post_obj->post_title; ?></div>
      </a> </li>
    <?php endif; ?>
    <li class="main col-md-4"> <a href="<?php echo $this_term_url; ?>">
      <div class="img" style="background-image:url(<?php echo $this_term_bg; ?>)">
        <h4>完整專題報導 </h4>
      </div>
      <div class="articleMainContent text-center"><?php echo $term_name; ?></div>
      </a> </li>
    <?php if(!empty($next_post_obj)): ?>
    <li class="col-md-4"> <a href="<?php echo $next_post_link; ?>">
      <div class="img" style="background-image: url(<?php echo get_photon_url($next_post_image_url[0]); ?>)">
        <h4>下篇文章 </h4>
      </div>
      <div class="articleMainContent"><?php echo $next_post_obj->post_title; ?></div>
      </a> </li>
    <?php endif; ?>
  </ul>
</div>