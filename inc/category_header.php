<div id="hero"></div>
  <div class="category-header">
    <div>
      <h1 class="category-name"><?php echo $headline_cat; ?></h1>
    </div>
<?php if( ! is_null($headline)): ?>
<?php
  define('HEADLINE_BG', $headline['full_featured_img']);
?>
<script>
jQuery(document).ready(function() {
    jQuery("#hero").ezBgResize({
    opacity : 0.9,
    center : true,
    img : "<?php echo HEADLINE_BG; ?>"
    });
});
</script>
    <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12">
      <div class="post-list-item">
          <a href="<?php echo $headline['permalink']; ?>"><img src="<?php echo $headline['thumbnail']; ?>" class="media-object">
          <div class="post-list-item-content">
            <h3><?php echo $headline['title']; ?></h3>
            <h6><abbr class="timeago" title="<?php echo date('c', $headline['timestamp']); ?>"><?php echo date('Y/m/d', $headline['timestamp']); ?></abbr> • <?php echo $headline['social_count']; ?><span class="glyphicon glyphicon-share" style="margin: 2px;"></span></h6>
          </div>
          </a>
      </div>
    </div>
<?php endif; ?>
  </div>
