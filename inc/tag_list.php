<!--
  <div class="alert" style="height:230px; background-color: #7DABD3;position: relative; color: #fff;text-align:center;line-height: 1.4em;
font-size:22x;font-weight:900;font-family:inherit;">
    <button style="position: absolute;top:5px;right:5px;" class="close" type="button" data-dismiss="alert">Close</button>
    <div style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/forum_logo.png);width:240px;height:60px; margin: 0 auto; background-repeat: no-repeat; "></div>
    <div style="margin-top: 15px;">關鍵論壇上線啦！讓我們輪流來「談」
    <a href="http://forum.thenewslens.com/?ref=forumpost" target="_blank">
    <button style="background: #7DABD3;border: 1px solid #fff;color: #fff;" type="button">立即去看看</button>
    </a>
    </div>  
  </div>
-->
<?php
  
  $posttags = get_the_tags();
  $tag__in = array();

  if ($posttags) {
    $output = '<hr><p><span class="glyphicon glyphicon-tags" style="margin-right: 10px;"></span>';
      foreach ($posttags as $tag) {
        $output .= '<span style="margin: 10px;"><a href="' . get_tag_link($tag->term_id) . '?ref=tagpost">&#149;&#32;' . $tag->name . '</a></span>';
        $tag__in[] = $tag->term_id;
      }

    $output .= '</p>';

    echo $output;
  }

  $extra_param = array(
    'tag__in' => $tag__in,
    'post__not_in' => array(get_the_ID()),
  );

  if(count($tag__in) > 0){
    $related_list_count = 4;
    $related_list = get_latest_list_data($related_list_count, 1, false, false, false, false, $extra_param);

    // sponsor content
    $sponsor_story = get_latest_list_data(1, 1, 'panasonic-viera');
    array_splice($related_list, 1, 0, $sponsor_story);
    if( count($related_list) > $related_list_count ){
      array_pop($related_list);
    }

    if(count($related_list) > 0){
      $rp_html = '<div class="tnl-subject" style="margin-bottom:40px;"><span>你可能也喜歡 You may also like</span><br><hr></div><div class="col-lg-12">';


      foreach ( $related_list as $data){

        $rp_html .= "<div class='col-lg-3 col-md-4 col-sm-6 col-xs-12' style='margin-bottom:40px;'><div><div class='post-cover' style=' background-size: 100%; background-image: url(\"" . $data['thumbnail'] . "\"); height:150px; clear: both; border-style: solid;border-width: 1px;border-bottom-width: 0px;border-color: #E9E9E9;'></div>";
        
        if( $data['is_sponsor'] ){
          $rp_html .= '<div style="color: #FFFFFF;font-size: 13px;line-height: 20px;text-align: center;background-color:#7DABD3;position: absolute;bottom: 5px;z-index: 5;width: 87%;">贊助文章</div>';
        }

        $rp_html .= "<div class='author-list-box' style='margin-top: -21px;border-style: solid;border-width: 1px;border-top-width: 0px;border-color: #E9E9E9;'><a href='" . $data['permalink'] . "?ref=relatedpost'><h4 class='text-center' style='line-height: 24px;'>" . $data['title'] . "</h4></a><p style='color: #8D8D8D;font-size: 13px;margin: 0 25px 15px 25px;line-height: 20px;'>" . summarize_long_text(str_replace(array("\n", "\r", "\r\n"), '',  $data['desc']), 34) . " ... </p></div></div></div>";

      }

      $rp_html .= '</div>';
      $rp_html .= '<span style="text-align:center;">
    <span class="glyphicon glyphicon-comment" style="margin-right: 10px;"></span>有話想說？　樓下見！
    <a href="#comment-panel" style="color:#fff"><button type="button" class="btn btn-success" style="width:100%;font-size: 22px;">評論 comments</button></a>
    </span>';
      
      echo wpautop($rp_html);
    }
  }
?>
