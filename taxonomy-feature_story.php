<?php

if(empty($GLOBALS['fs_config'])) return;

$cur_term = get_query_var('feature_story');
$cur_term_obj = get_term_by('slug', $cur_term, 'feature_story');
$term_name = strtoupper($cur_term_obj->name);
$term_link = get_tag_link($cur_term_obj->term_id);

$guide_filter = array(
  'feature_story' => $cur_term_obj->slug,
  'meta_query' => array(
    array(
      'key' => 'fs_type',
      'value' => 'guide',
    )
  ),
);

$guide_list = get_latest_list_data(-1, 1, false, false, false, false, $guide_filter);

$head_guide = $guide_list[0];
$other_guide = array_slice($guide_list, 1);

$regular_filter = array(
  'feature_story' => $cur_term_obj->slug,
  'meta_query' => array(
    array(
      'key' => 'fs_type',
      'value' => 'regular',
    )
  ),
);

$regular_list = get_latest_list_data(-1, 1, false, false, false, false, $regular_filter);

$head_regular = $regular_list[0];
$other_regular = array_slice($regular_list, 1);


?>
<?php get_header(); ?>
<style type="text/css">
@media (max-width: 550px) {
 h3{    
    font-size: 15px;
    margin:5% 5% 2% 5%;
    margin-top: 10px;
  }
}  
</style>

<div class="topics">

<div id="hero" style="background-image:url(<?php echo (!empty($GLOBALS['fs_config'][$term_name]['bg_img']))?(get_photon_url($GLOBALS['fs_config'][$term_name]['bg_img'])):''; ?>);overflow:auto; ">
  <div class="hero-content" style="height:auto;">
    <div class=""> 
      <div class="text-center">
        <h3><?php echo $term_name; ?></h3>
        <?php if(!empty($cur_term_obj->description)): ?>
        <div style="height: 100%;overflow: auto;"><?php echo $cur_term_obj->description; ?></div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<?php if(!empty($GLOBALS['fs_config'][$term_name]['sponsor_text'])): ?>
<div style="padding: 13px;font-weight: 900;" class="tnl-subject-wrap content text-center"><span class="Sponsored">議題贊助：</span> <?php echo $GLOBALS['fs_config'][$term_name]['sponsor_text']; ?>
</div>
<?php endif; ?>
<?php if(!empty($guide_list)): ?>
<div class="HolyGrail-body tnl-subject-wrap content">
  <main class="HolyGrail-content">
    <?php if(!empty($head_guide)): ?>
    <div class="">
      <div class="articleMain">
        <div class="img col-md-6 post-cover" style="background-image:url(<?php echo $head_guide['full_featured_img']; ?>)">
          <div class="iconArticle">導讀<br>文章</div>
        </div>
        <div class="articleMainContent col-md-6">
          <h3 class="text-center" ><a href="<?php echo $head_guide['permalink']; ?>" ><?php echo $head_guide['title']; ?></a></h3>
          <div class="tab-pane fade active in"><?php echo wpautop($head_guide['excerpt']); ?></div>  
        </div>
        <a href="<?php echo $head_guide['permalink']; ?>" class="btnReadMore">閱讀全文</a> 
      </div>
    </div>
    <hr>
    <?php endif; ?>
    <?php if(!empty($other_guide)): ?>
    <div style="background-color: rgb(240, 240, 240);overflow: auto;overflow-x:hidden;overflow-y:auto;padding-bottom: 0px;overflow-x:hidden;overflow-y:auto;" class="tnl-subject-wrap content">
      <div class="listArticle">
        <main class="HolyGrail-content">
        <ul class="listArticle article7">
          <?php foreach($other_guide as $data): ?>
          <li class="col-md-6">
            <a href="<?php echo $data['permalink']; ?>" class="img" style="background-image: url(<?php echo $data['full_featured_img']; ?>)">
            <div class="iconArticle bgOrange">導讀<br>文章</div>
            </a>
            <div class="articleMainContent">
              <h3 class="text-center"><a href="<?php echo $data['permalink']; ?>" ><?php echo $data['title']; ?></a></h3>
            </div>
          </li>
          <?php endforeach; ?>
        </ul>
        </main>
      </div> 
    </div>
    <?php endif; ?>
  </main>
</div>
<?php endif; ?>
<?php if(!empty($GLOBALS['fs_config'][$term_name]['sponsor_banner1'])): ?>

<?php
        $sp1 = dirname( __FILE__ ) . '/sponsor/feature_story/' . $GLOBALS['fs_config'][$term_name]['sponsor_banner1'];
        if(is_file($sp1)){
            require($sp1);
        }
?>
<?php else: ?>
      <div class="tnl-panel">
        <?php require('inc/SF_STORY_BELOW-TITLE_728x90.php'); ?>
      </div>
<?php endif; ?>

<?php if(!empty($regular_list)): ?>
<div style="background-color: rgb(240, 240, 240);" class="tnl-subject-wrap content">
  <?php if(!empty($head_regular)): ?>
  <div class="listArticle">
    <main class="HolyGrail-content">
      <ul class="listArticle article7">
        <li >
          <a href="<?php echo $head_regular['permalink']; ?>" class="img" style="background-image: url(<?php echo $head_regular['full_featured_img']; ?>)">
          </a>
          <div class="tab-pane fade active in">
            <h3 class="text-center" ><a href="<?php echo $head_regular['permalink']; ?>" ><?php echo $head_regular['title']; ?></a></h3>
            <?php if($data['is_sponsor'] == 1): ?>
            <div style="color: #FFFFFF;font-size: 13px;line-height: 20px;text-align: center;background-color:#7DABD3;margin:0 auto;width:200px;">贊助文章</div>
            <?php endif; ?>
            <?php echo wpautop($head_regular['excerpt']); ?>
          </div>
          <a href="<?php echo $head_regular['permalink']; ?>" class="btnReadMore">閱讀全文</a> </li>
      </ul>
    </main>
  </div>
  <hr>
  <?php endif; ?>
  <?php if(!empty($other_regular)): ?>
  <div class="">
    <ul class="listArticle article7">
      <?php foreach($other_regular as $data): ?>
      <li class="col-md-3">
        <a href="<?php echo $data['permalink']; ?>" class="img" style="background-image: url(<?php echo $data['full_featured_img']; ?>)">
        </a>
        <div class="articleMainContent" style="height:60px">
          <h3><a href="<?php echo $data['permalink']; ?>" ><?php echo $data['title']; ?></a></h3>
        </div>
        <?php if($data['is_sponsor'] == 1): ?>
        <div style="color: #FFFFFF;font-size: 13px;line-height: 20px;text-align: center;background-color:#7DABD3;position: absolute;bottom: 5px;z-index: 5;width: 90%;">贊助文章</div>
        <?php endif; ?>
      </li>
      <?php endforeach; ?>
      <li class="col-md-3">
        <div id="ad_container">
      <?php if(!empty($GLOBALS['fs_config'][$term_name]['sponsor_banner2'])): ?>
      <?php
              $sp2 = dirname( __FILE__ ) . '/sponsor/feature_story/' . $GLOBALS['fs_config'][$term_name]['sponsor_banner2'];
              if(is_file($sp2)){
                  require($sp2);
              }
      ?>
      <?php else: ?>
            <div class="tnl-panel">
              <?php require('inc/SF_STORY_SIDEBAR-HEAD_300x250.php'); ?>
            </div>
      <?php endif; ?>
        </div>
      </li>
    </ul>
  </div>
  <br clear="all" />
  <?php endif; ?>
</div>
<?php endif; ?>
</div>

<?php get_footer(); ?>