<?php
/*
    Template Name: rss list
*/

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

$headline_list = get_latest_list_data(1, 1, false, CATE_NEWS_ID);
$headline = array_pop($headline_list);
$headline_cat = '新聞';

?>

<?php get_header(); ?>


  <div class="HolyGrail-body-post">

    <main class="HolyGrail-content-post" style="margin-top:0;">
       
      <div class="category-nav" style="">
      <div class="col-lg-12">
        <div class="newest-title">
          <h1 style="padding: 15px 31px 10px 0px; max-width:250px; color:rgb(117, 117, 117);">最新文章 RSS</h1>
        </div>

      </div>
      <div class="clearfix"></div>

      </div>
<div class="category-nav" style="border-bottom: 0px;
min-height: 8px !important;
margin-top: -17px;
margin-bottom: 10;
color: #ADADAD;">

</div>
<div class="post-list-item">
  <div class="col-lg-6" style="padding-bottom:25px;">
  <span ><img src="<?php echo get_template_directory_uri(); ?>/img/tnl-rss.png"><strong>什麼是RSS?</strong><br>

RSS（Really Simple Syndication）是一個簡單的資訊整合技術，它可以讓您在不用隨時拜訪我們網站的情況下，卻能夠即時收到您所訂閱的最新文章類別更新通知。
<br><br>

<strong>為什麼要訂閱RSS?</strong><br>

因為這樣可以有效節省您的時間，不需要分別去瀏覽您最喜愛的20個網站內容，而只要訂閱該網站的RSS，就可以將您訂閱的所有內容整合在同一個RSS閱讀器下追蹤。</span>
</div>
  <div class="panel panel-default post-list-item col-lg-6" style="">
    <!-- List group -->
    <ul class="list-group ">

      <li class="list-group-item"><a href="http://feeds.feedburner.com/TheNewsLens"><img style="width: 21px; height: auto; margin-right: 4px; border-radius: 2px;" src="<?php echo get_template_directory_uri(); ?>/img/RSS_Feed.png">全部文章 <button type="button" class="btn btn-warning btn-xs pull-right" style="margin-top:-1px;">訂閱</button></a></li>

      <li class="list-group-item"><a href="http://feeds.feedburner.com/thenewslens/news"><img style="width: 21px; height: auto; margin-right: 4px; border-radius: 2px;" src="<?php echo get_template_directory_uri(); ?>/img/RSS_Feed.png">新聞 <button type="button" class="btn btn-warning btn-xs pull-right" style="margin-top:-1px;">訂閱</button></a></li>

      <li class="list-group-item"><a href="http://feeds.feedburner.com/thenewslens/review"><img style="width: 21px; height: auto; margin-right: 4px; border-radius: 2px;" src="<?php echo get_template_directory_uri(); ?>/img/RSS_Feed.png">評論 <button type="button" class="btn btn-warning btn-xs pull-right" style="margin-top:-1px;">訂閱</button></a></li>

      <li class="list-group-item"><a href="http://feeds.feedburner.com/thenewslens/video"><img style="width: 21px; height: auto; margin-right: 4px; border-radius: 2px;" src="<?php echo get_template_directory_uri(); ?>/img/RSS_Feed.png">影片 <button type="button" class="btn btn-warning btn-xs pull-right" style="margin-top:-1px;">訂閱</button></a></li>
    </ul>
  </div>
</div>
<div class="clearfix"></div>


  
    </main>
<?php get_template_part( 'sidebar' ); ?>

  </div>

<?php get_footer(); ?>