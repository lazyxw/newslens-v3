
<?php get_header(); ?>

<?php

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

$cur_tag = get_query_var('tag');
$cur_tag_obj = get_term_by('slug', $cur_tag, 'post_tag');
$tag_name = strtoupper($cur_tag_obj->name);
$tag_link = get_tag_link($cur_tag_obj->term_id);
$active_class = ' class="active"';

if( 'n' == $_GET['st'] ){
  $extra_param = array( 'category__and' => array( CATE_NEWS_ID ));
} else if ( 'r' == $_GET['st'] ) {
  $extra_param = array( 'category__not_in' => array( CATE_NEWS_ID ));
} else {
  $extra_param = false;
}

$headline_list = get_latest_list_data(1, 1, $cur_tag_obj->slug, false, false, false, $extra_param);
$headline = array_pop($headline_list);
$headline_cat = $tag_name;


// sponsor tag list
$sponsor_tags = array(
 
  '城市作者' => array(
    'sponsor_id' => 'City_Main',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
      每一座城市都有自己的個性，代表了每一個不同的靈魂。它有它自己的獨特味道與文化，訴說著不同的故事。而人生最大的樂趣，也許就是在這些巷弄之中探索一個全新的世界。請與我們分享，投稿你的作品至contact@thenewslens.com。
    </div>',
    ),
);

$is_sponsor_tag = false;
$current_sponsor = '';

foreach( $sponsor_tags as $t => $a ){
  if( $t == $tag_name ){
    $is_sponsor_tag = true;
    $current_sponsor = $a;
    break;
  }
}

if($is_sponsor_tag){
  $sponsor_id = $current_sponsor['sponsor_id'];

  $page_title_height = '230';
  $page_desc = $current_sponsor['sponsor_text'];

  $bg_file = '/sponsor/tag/' . $sponsor_id . '.jpg';
  if(is_file(dirname( __FILE__ ) . $bg_file)){
    $page_title_bg = "background-image:url('" . get_template_directory_uri() . $bg_file . "'); background-size:100% auto;height:auto;";
  } else {
    $page_title_bg = '';
  }
} else {
  $page_title_height = '120';
  $page_title_bg = '';
  $page_desc = '';
}

?>
    <div class=" hero-content" style="position: absolute; color: white;  width: 100%;">
      <div style="width:400px; height:10%; background-color:#fff;"></div>
    </div>
    <div class="category-header text-center"  style="verticle-align:middle; height:<?php echo $page_title_height; ?>px; font-size:35px; padding-top:37px;<?php echo $page_title_bg; ?>">
<?php

    $page_title = wp_title( '-', false);
    $title = explode('-', $page_title); 
    echo $title[0];
    echo $page_desc;
?>
    </div>

  <div class="HolyGrail-body-post tnl-container" style="margin-top:0;">

    <main class="HolyGrail-content-post" style="margin-top:0;">
       
      <div class="category-nav" style="">
      <div class="col-lg-12">

        <div class="newest-selection">

          <ul class="nav nav-pills" style="margin-top:20px; font-weight:900;">
          <li<?php echo ($extra_param == false)?$active_class:''; ?>><a href="<?php echo $tag_link; ?>">全部文章</a></li>
           <li><a href="http://www.thenewslens.com/tag/上海作者">上海</a></li>
           <li><a href="http://www.thenewslens.com/tag/北京作者">北京</a></li>           
           <li><a href="http://www.thenewslens.com/tag/伊斯坦堡作者">伊斯坦堡</a></li>
           <li><a href="http://www.thenewslens.com/tag/安曼作者">安曼</a></li>
           <li><a href="http://www.thenewslens.com/tag/東京作者">東京</a></li>
           <li><a href="http://www.thenewslens.com/tag/矽谷作者">矽谷</a></li>
           <li><a href="http://www.thenewslens.com/tag/柏林作者">柏林</a></li>
           <li><a href="http://www.thenewslens.com/tag/耶路撒冷作者">耶路撒冷</a></li>
           <li><a href="http://www.thenewslens.com/tag/香港作者">香港</a></li>                   
           <li><a href="http://www.thenewslens.com/tag/開羅作者">開羅</a></li>
           <li><a href="http://www.thenewslens.com/tag/新漢堡作者">新漢堡</a></li>
           <li><a href="http://www.thenewslens.com/tag/新德里作者">新德里</a></li>
           <li><a href="http://www.thenewslens.com/tag/澳門作者">澳門</a></li>
           <li><a href="http://www.thenewslens.com/tag/羅馬作者">羅馬</a></li>
        </ul>
      </div>

      </div>
      <div class="clearfix"></div>

      </div>

    <div class="category-nav" style="border-bottom: 0px;
    min-height: 8px !important;
    margin-top: -17px;
    margin-bottom: 10;
    color: #ADADAD; width:100%">
    <span style="float:right; margin-right:25px;">第 <?php echo $paged; ?> 頁 </span>
    </div>
<?php
$item_count = 10;


$story_list = get_latest_list_data($item_count, $paged, $cur_tag_obj->slug, false, false, false, $extra_param);

if ( count($story_list) > 0 ) {
  $postCount = 0;
  foreach ( $story_list as $data){
    get_post_list_item_html($data, 'r');
  }
} else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
}

?>
          <div class="pager-div" style="text-align:center;border-top: 1px solid #eee;">
            <?php wpbeginner_numeric_posts_nav(get_latest_list_data($item_count, $paged, $cur_tag_obj->slug, false, false, true, $extra_param), true); ?>
          </div>
          <div class="bs-callout bs-callout-info" style="margin-top: 40px;">
            <?php require('inc/SF_STORY_BELOW-TITLE_728x90.php'); ?>
          </div>
    </main>

    <aside class="HolyGrail-right hidden-mid tnl-subject-wrap" style="background-color:rgb(230, 230, 230);">
<?php if($is_sponsor_tag): ?>
<?php
        $sp1 = dirname( __FILE__ ) . '/sponsor/tag/' . $sponsor_id . '1.php';
        if(is_file($sp1)){
            require($sp1);
        }
?>
<?php else: ?>
      <div class="tnl-panel">
        <?php require('inc/SF_STORY_SIDEBAR-HEAD_300x250.php'); ?>
      </div>
<?php endif; ?>
      <?php require('inc/follow_us.php'); ?>
<?php if($is_sponsor_tag): ?>
<?php
    $sp2 = dirname( __FILE__ ) . '/sponsor/tag/' . $sponsor_id . '2.php';
    if(is_file($sp2)){
        require($sp2);
    }
?>
<?php endif; ?>
      <div class="tnl-panel">
        <?php require('inc/hot_post.php'); ?>
      </div>
    
    </aside>

  </div>

<?php get_footer(); ?>