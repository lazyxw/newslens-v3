<?php

$content = get_the_content();
$content = '<a id="dd_start"></a>' . $content . "\n<a id=\"dd_end\"></a>";

$featured_img_id = get_post_thumbnail_id($post->ID);
$featured_img_obj = get_post($featured_img_id);
$photo_credit = strip_tags($featured_img_obj->post_excerpt);
$cover_photo = get_photon_url($featured_img_obj->guid);

?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <main class="HolyGrail-content-post">
    <div class="post-cover" style="background-image: url('<?php echo $cover_photo; ?>');">
      <div class="post-social-horizon">
        <?php require('inc/share.php'); ?>
      </div>
    </div>
    <div class="photo-credit" style=""><?php echo $photo_credit; ?></div>

    <div>
        <h1 class="text-center"><?php the_title(); ?></h1>
        <h5 class="post-info text-center"><?php coauthors_posts_links(' ', ' '); ?> <time class="entry-date" datetime="<?php echo date(DATE_W3C); ?>" ><?php the_time(get_option('date_format')) ?></time> 發表於
        <?php
        $categories = get_the_category();
        $cate_container = array();

        if($categories){
            foreach($categories as $category) {
                if ($category->slug != 'featured' and $category->slug != 'news') { //ignore the featured category name
                    $cate_name = $category->name;

                    $parent = get_category($category->category_parent);

        ?>
                    <span><a class="post-cat-box" href="<?php echo get_category_link($category->cat_ID); ?>">&#149;&#32;<?php echo $cate_name; ?></a></span>
        <?php
                    $cate_container[] = $category->name;
                }
            }
        }
        ?>
        </h5>
        <?php if(!empty($post->post_excerpt) && (in_category('*新聞'))): ?>
        <div class="tnl-panel" style="min-height:130px;margin: 0 10%;background-color: rgb(49, 184, 248);">
            <div style="color: #fff; margin: 14px; font-size: 21px; font-weight: 600; font-style: italic;">
                <img src="http://www.thenewslens.com/wp-content/uploads/2014/06/TNL-logo-icon.png" scale="0" style="width: 22px;height: auto;margin-bottom: 3px;margin-right: 5px;margin-left: 10px;">你為什麼需要知道這則新聞
            </div>

            <div style="color: #F5F5F5;">               
                <p><?php the_excerpt(); ?></p>
            </div>
            
        </div>
        <?php endif; ?>
        <div class="tnl-content tnl-subject-wrap">

        <?php 
            $en_content = get_post_meta( get_the_ID(), 'en_content', true );

            if(!empty($en_content)){
        ?>
        <div style="padding-top: 29px;">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#zh-content" data-toggle="tab">中文</a></li>
                <li><a href="#en-content" data-toggle="tab">English</a></li>
            </ul>
        </div>
        <?php
            }
        ?>

        <div class="clearfix"></div>



        <div id="entry-content" class="media-body entry-content tab-content" style="font-size:17px; padding:10px">
            <div class="tab-pane fade active in" id="zh-content">
                <?php

                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]&gt;', $content);
                    $content_container = explode("</p>", $content);

                    $param_count = count($content_container);
                    $param_count = $param_count;
                    $ad_in_count = round($param_count / 2);
                    foreach ($content_container as $key => $value) {
                        if( !in_array('影片', $cate_container) && is_singular() ){
                            if( $param_count > 7 and $ad_in_count == $key )
                                require('inc/SF_STORY_CONTENT-INSIDE_300x250.php');
                        }



                        echo $value . '</p>';
                    }

                    require('inc/feature_story_nav.php');
                    require('inc/tag_list.php');
                    require('inc/fan_page_like.php');
                ?>
            </div>
            <?php if(!empty($en_content)): ?>
            
            <div class="tab-pane fade" id="en-content">
                <?php
                $en_content = apply_filters('the_content', $en_content);
                $en_content = str_replace(']]>', ']]&gt;', $en_content);
                    
                echo $en_content; 
                ?>
            </div>
            
            <?php endif;?>
            <div class="bs-callout bs-callout-info">
    				<?php require('inc/SF_STORY_BELOW-CONTENT_728x90.php'); ?>
    		</div>

        </div>


      </div>

<?php get_template_part( 'author-bio' ); ?>


    <a name="comment-panel"></a>
    <div style="width:80%;margin:0 auto;">
    <?php 
    $this_post_time = get_post_time('U', true);
    $disable_disqus_start = strtotime('2014/06/09 00:00:00');
    $disable_disqus_end = strtotime('2014/06/10 04:00:00');


    if( $this_post_time > $disable_disqus_start and $this_post_time < $disable_disqus_end){
        echo '<p>因為留言系統發生錯誤，本文章的回應功能暫時關閉。</p>';
    }else{
        comments_template();
    }
    ?>
    </div>

    </main>
    </article>
