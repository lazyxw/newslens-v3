
<?php get_header(); ?>

<?php

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

$cur_tag = get_query_var('tag');
$cur_tag_obj = get_term_by('slug', $cur_tag, 'post_tag');
$tag_name = strtoupper($cur_tag_obj->name);
$tag_link = get_tag_link($cur_tag_obj->term_id);
$active_class = ' class="active"';

if( 'n' == $_GET['st'] ){
  $extra_param = array( 'category__and' => array( CATE_NEWS_ID ));
} else if ( 'r' == $_GET['st'] ) {
  $extra_param = array( 'category__not_in' => array( CATE_NEWS_ID ));
} else {
  $extra_param = false;
}

$headline_list = get_latest_list_data(1, 1, $cur_tag_obj->slug, false, false, false, $extra_param);
$headline = array_pop($headline_list);
$headline_cat = $tag_name;


// sponsor tag list
$sponsor_tags = array(
  'PM2.5' => array(
    'sponsor_id' => 'pm2-5',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">你一天要呼吸幾次？你知道你很有可能吸入空氣中隱形的健康殺手？

天空灰灰的，你可能以為是「霧」，其實是「霾」！

PM2.5已經成為全球高度關注的新興污染物。它是空氣中極微小的懸浮粒子，直徑僅有2.5微米。而這些微小的粒子負載了重金屬、戴奧辛和病菌，如果被人體吸入，PM2.5可直接到肺部的最深處，進入肺泡甚至血管循環中，大幅增加了心血管、肺線癌罹患率，和呼吸系統疾病的可能性。如果空氣中PM2.5濃度過高，容易造成氣喘，孕婦早產。

連呼吸都有風險，甚至會致癌，你還能不了解PM2.5嗎？

此專區帶你認識PM2.5，究竟會對健康造成什麼危害，全球都在關心？各國監測標準比一比，而台灣又是如何監測？你必須關心的健康議題－PM2.5。</div>',
    ),

  '爵士樂' => array(
    'sponsor_id' => 'jazz',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">爵士樂 其實離你我很近
你期待爵士樂，跟你的連結是什麼？你認為爵士樂，可以在台北，扮演什麼樣的位置？爵士樂，其實可以不那麼洋、那麼舶來，在台灣，有著相伴著你我記憶的片段，只是我們不知道原來那就是爵士樂；在台灣，也有許多優秀的本地樂手，在都會的轉角認真地演出與創作，值得你我更多的聆聽；在台灣，我們其實也能將自己的故事，透過爵士樂這個觸媒，向全世界散播，形塑出屬於我們的爵士樂特色。
由台北市政府文化局主辦的2014年臺北爵士音樂節，於7月19日及7月20日，於大安森林公園露天音樂台，「以世代傳承與創新」為主題規劃共7場音樂會演出。台上台下最無距離的「臺北爵士音樂節」國際性音樂盛會，將再度呈現在台北的星空下。
www.taipeijazzfestival.com</div>',
    ),

  '台灣大學作者' => array(
    'sponsor_id' => 'Campus_NTU',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
     台灣大學為一綜合研究型大學，規模、領域完整，師資及學生素質優異，校務發展在秉持核心價值「敦品勵學、愛國愛人」之理念下，以教育卓越、學術卓越及社會關懷三大主軸， 營造優質學習環境以培育社會菁英。本校訂定「教育卓越、多元學習」、「延攬教學、研究優質人才」、「強化基礎建設與軟硬體環境」、「卓越與前瞻研究」、「學術國際化」及「行政精進」等六項， 作為校務發展之關鍵策略， 以提升教學、研究、學生事務、國際化、基礎建設及軟硬體環境。提供師生一流學習與創新的優質環境，培養社會領導菁英與知識創新研究人才、提升臺灣學術水準，並協助國家經濟發展，解決人類永續發展重大問題，達到「華人頂尖、世界一流」大學之願景。
     </div>',
    ),

  '政治大學作者' => array(
    'sponsor_id' => 'Campus_ChengChi',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
     政治大學自民國 16 年創立，民國 43 年在台復校， 迄今歷經 85 年，其間雖經改制、遷校，但始終秉持和諧、自主、均衡、卓越之創校宗旨，不斷精進教學 及學術研究，以期配合國家建設、社會發展的需要， 培育優秀人才。
     </div>',
    ),

  '清華大學作者' => array(
    'sponsor_id' => 'Campus_ChingDa',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
     國立清華大學立校於中西文化激盪之際，不同學問的思辨，本土與國際的對話，滋養人才，學貫中西，貢獻人群，橫跨學術、藝術、文學、軍事、政治、經濟各領域，為本校傲人的傳統。學校歷經變遷，而弦歌不曾稍歇；於逆境中，自強以不息，於板蕩中，厚德以載物，是本校師生的精神與面貌，以迄於今。 ─ 清大校長賀陳弘
     </div>',
    ),

  '中山大學作者' => array(
    'sponsor_id' => 'Campus_ZhongShan',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
     中山大學位於高雄市西子灣，東毗壽山，西臨台灣海峽，南通高雄港，北跨柴山，依山面海，碧波萬頃，水天相接，自由學風，建校三十餘年來已成為一所精緻的研究型綜合大學。未來將持續追求卓越、邁向頂尖，型塑「山海胸襟  熱情洋溢」的特色品牌，積極塑造一個以「人」為本的優質校園文化，並營造「樂在其中」的工作氛圍，使中山大學成為莘莘學子嚮往，優秀人才聚集，校友引以為傲，社會高度認同的國際知名一流大學。</div>',
    ),

   '師範大學作者' => array(
    'sponsor_id' => 'Campus_ShiFan',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
     國立師範大學自民國83年起，為因應「師資培育法」實施，積極轉型發展為綜合大學，成為博、碩、學士班正規生逾萬人之多元化綜合大學。展望未來，本校將在既有豐沛的人文基礎上，植入現代科技知識，持續深耕發揚本校特色領域，並加強國際化、資訊化、企業化，讓今日的師大人，都能成為明日的大師，使本校成為具「古典風華、現代視野」之亞洲一流，世界馳名之綜合大學。
     </div>',
    ),

  '義守大學作者' => array(
    'sponsor_id' => 'Campus_YiShou',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
     義守大學的使命為建構臻善的學習場域與多元的學習典範，提供永續學習之優質高等教育，以培育學行兼備之人才；發展具特色與專業智識之學術與產業應用研究，強化學術社群、產業及社區之聯合與合作，以成為知識創新、傳播與應用之共振平台；增進國際交流與合作，以拓展所有成員國際宏觀之視野及全球議題之關注；營造專業、尊重、合作與倫理精神的環境，實現成員的知識與人生成就，以提升組織成員對全體社會之服務與貢獻。並建立一所「創新知識、服務社會、永續發展」之國際化綜合大學。
     </div>',
    ),

  '東吳大學作者' => array(
    'sponsor_id' => 'Campus_DonHu',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
     東吳大學於1900年由基督教監理會在中國蘇州創辦，是中國第一所西制大學。1951年在台灣復校，也是台灣第一所私立大學。秉持中、西文校訓「養天地正氣、法古今完人」、「Unto a Full-Grown Man」的精神，致力發展成一所精緻、有特色、具前瞻性的優質教學大學，培育兼備專業與通識，富創意及執行力之社會中堅人才。展望未來，在本校優質教學的深厚基礎上，以整合、創新的概念，發揮綜效建立特色，以期在我國高等教育界建立卓然的領先地位。
     </div>',
    ),

  '台北大學作者' => array(
    'sponsor_id' => 'Campus_TaipeiUni',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
     台北大學以「追求真理、服務人群」為治學願景，沿襲建校以來之優秀傳統，致力於培養社會與企業所需之人才，並以「專業」、「人際」、「倫理」、「國際觀」四大素養為概念，承接治學願景並開展具體能力。為求有效縮減學校教育與社會、職場所需能力的落差，本校以培養學生四大素養為基石，具體提出學生應具備之八大核心能力：創意思考與問題解決、綜合統整、溝通協調、團隊合作、誠信正直、尊重自省、多元關懷及跨界合作。透過以「情境學習」為主軸的三項分項計畫，將此八大核心能力與本次教學卓越計畫之想望成果具體連結，期望不僅強化教與學之品質，亦協助學生建立「可攜式」能力，使得在校所學能成為一生受用之資產。 
     </div>',
    ),

  '輔仁大學作者' => array(
    'sponsor_id' => 'Campus_FuLen',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
     輔仁大學是天主教會在我國設立之第一所大學，創校迄今近九十年，向本敬天愛人之精神，為全人教育而努力，除了希望為國家培植德、智、體、群、美五育兼備之英才， 亦願以各種學術活動及社區服務、貢獻社會人群，並以客 觀執著之研究，致力於真理之追求及中西文化之交融，俾達增進人類福祇，促進世界大同之目標。本校在培育人才方面，兼注通才與專才養成教育，尤其致力人文精神的培養，期使學生在就業之餘，能有高尚的情 操與豐富的人生。此外，為擴大接觸層面，本校並與多所 外國知名大學締結「姐妹校」關係，以增進文化交流，並提昇學校的國際地位。
     </div>',
    ),

  '中國醫藥大學作者' => array(
    'sponsor_id' => 'Campus_ChineseMedical',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
     本校依據校務發展計畫及學校自我定位，擬訂定學生基本素養及核心能力，以落實本校願景與使命。本校校訓「仁、慎、勤、廉」是學生基本素養之最高原則，以培育視病猶親、修己善群、終身學習、術德兼修的學子，提供中西醫藥等專業之優異人才，照顧個人、家庭及社區，服務社會，以期符合教育部所倡導之「全人」教育理念；使本校學生除獲得正規專業知能外，復能拓展人文、社會、哲學、藝術、倫理、自然科學等現代公民應具備的知識及文化涵養，以因應未來社會變遷並邁向理想的人生。此一校訓已有50多年歷史，依然符合西方現代醫學教育之目標：利他(Altruistic)的價值觀、成熟(Skillful)的醫術技能、豐富(Knowledgeable)的醫學知識、親切負責(Dutiful)的態度。
     </div>',
    ),

  '哈佛大學作者' => array(
    'sponsor_id' => 'Campus_Harvard',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
     哈佛於1636年由當地的殖民地立法機關立案成立，迄今為全美歷史最悠久的高等學府，並擁有北美最古老的校董委員會。[10] 其最初稱之為「新學院」，該機構為了感謝一名年輕的牧師約翰·哈佛所作出的捐贈，而改名為「哈佛學院」。
     </div>',
    ),

  '密西根大學作者' => array(
    'sponsor_id' => 'Campus_Michigan',
    'sponsor_text' => '<div style="width: 80%;margin: 10px auto;font-size: 14px;background-color: rgba(0, 0, 0, 0.39);padding: 15px;color: #FFFAFA;border-radius: 5px;">
     密西根大學是美國密西根州的一所著名公立大學。它有三個校區，分別是安娜堡（主校區，又譯安阿伯）、迪爾伯恩(Dearborn)和弗林特 (Flint) [5] 。密西根大學於1817年建校，是美國歷史最悠久的大學之一，在世界範圍內享有盛譽。密大建校以來，在各學科領域中成就卓著並擁有巨大影響，多項調查顯示該大學超過70％的專業排在全美前10名，被譽為「公立常春藤」，與加州大學伯克利分校以及威斯康辛大學素有「公立大學典範」之稱。
     </div>',
    ),

    '北京作者' => array(
    'sponsor_id' => 'City_Beijing',
    ),

    '柏林作者' => array(
    'sponsor_id' => 'City_Berlin',
    ),

    '新漢堡作者' => array(
    'sponsor_id' => 'City_SaoPaulo',
    ),

     '開羅作者' => array(
    'sponsor_id' => 'City_Cario',
    ),

     '英國作者' => array(
    'sponsor_id' => 'City_England',
    ),

   '德國作者' => array(
    'sponsor_id' => 'City_Germany',
    ),

     '荷蘭作者' => array(
    'sponsor_id' => 'City_Holand',
    ),

     '香港作者' => array(
    'sponsor_id' => 'City_HongKong',
    ),

      '新德里作者' => array(
    'sponsor_id' => 'City_India',
    ),

    '耶路撒冷作者' => array(
    'sponsor_id' => 'City_Isreal',
    ),

     '東京作者' => array(
    'sponsor_id' => 'City_Tokyo',
    ),

      '安曼作者' => array(
    'sponsor_id' => 'City_Jordan',
    ),

       '堪薩斯作者' => array(
    'sponsor_id' => 'City_Kansas',
    ),

      '倫敦作者' => array(
    'sponsor_id' => 'City_London',
    ),

        '澳門作者' => array(
    'sponsor_id' => 'City_Macau',
    ),

      '密西根大學作者' => array(
    'sponsor_id' => 'Campus_Michigan',
    ),

       '紐約作者' => array(
    'sponsor_id' => 'City_NewYork',
    ),

      '羅馬作者' => array(
    'sponsor_id' => 'City_Rome',
    ),

       '上海作者' => array(
    'sponsor_id' => 'City_Shanghai',
    ),

       '矽谷作者' => array(
    'sponsor_id' => 'City_Silicone',
    ),

    '弗利堡作者' => array(
    'sponsor_id' => 'City_Switzerland',
    ),

    '伊斯坦堡作者' => array(
    'sponsor_id' => 'City_Istanbul',
    ),

);

$is_sponsor_tag = false;
$current_sponsor = '';

foreach( $sponsor_tags as $t => $a ){
  if( $t == $tag_name ){
    $is_sponsor_tag = true;
    $current_sponsor = $a;
    break;
  }
}

if($is_sponsor_tag){
  $sponsor_id = $current_sponsor['sponsor_id'];

  $page_title_height = '230';
  $page_desc = $current_sponsor['sponsor_text'];

  $bg_file = '/sponsor/tag/' . $sponsor_id . '.jpg';
  if(is_file(dirname( __FILE__ ) . $bg_file)){
    $page_title_bg = "background-image:url('" . get_template_directory_uri() . $bg_file . "'); background-size:100% auto;height:auto;";
  } else {
    $page_title_bg = '';
  }
} else {
  $page_title_height = '120';
  $page_title_bg = '';
  $page_desc = '';
}

?>
    <div class=" hero-content" style="position: absolute; color: white;  width: 100%;">
      <div style="width:400px; height:10%; background-color:#fff;"></div>
    </div>
    <div class="category-header text-center"  style="verticle-align:middle; height:<?php echo $page_title_height; ?>px; font-size:35px; padding-top:37px;<?php echo $page_title_bg; ?>">
<?php

    $page_title = wp_title( '-', false);
    $title = explode('-', $page_title); 
    echo $title[0];
    echo $page_desc;
?>
    </div>

  <div class="HolyGrail-body-post tnl-container" style="margin-top:0;">

    <main class="HolyGrail-content-post" style="margin-top:0;">
       
      <div class="category-nav" style="">
      <div class="col-lg-12">

        <div class="newest-selection">

          <ul class="nav nav-pills" style="margin-top:20px; font-weight:900;">
          <li<?php echo ($extra_param == false)?$active_class:''; ?>><a href="<?php echo $tag_link; ?>">全部文章</a></li>
          <li<?php echo ($_GET['st'] == 'n')?$active_class:''; ?>><a href="<?php echo $tag_link; ?>?st=n">新聞</a></li>
          <li<?php echo ($_GET['st'] == 'r')?$active_class:''; ?>><a href="<?php echo $tag_link; ?>?st=r">評論</a></li>
        </ul>
      </div>

      </div>
      <div class="clearfix"></div>

      </div>

    <div class="category-nav" style="border-bottom: 0px;
    min-height: 8px !important;
    margin-top: -17px;
    margin-bottom: 10;
    color: #ADADAD; width:100%">
    <span style="float:right; margin-right:25px;">第 <?php echo $paged; ?> 頁 </span>
    </div>
<?php
$item_count = 10;


$story_list = get_latest_list_data($item_count, $paged, $cur_tag_obj->slug, false, false, false, $extra_param);

if ( count($story_list) > 0 ) {
  $postCount = 0;
  foreach ( $story_list as $data){
    get_post_list_item_html($data, 'r');
  }
} else {
?>
        <div class="post-list-item">
          <h3>沒有相關文章</h3>
        </div>
<?php
}

?>
          <div class="pager-div" style="text-align:center;border-top: 1px solid #eee;">
            <?php wpbeginner_numeric_posts_nav(get_latest_list_data($item_count, $paged, $cur_tag_obj->slug, false, false, true, $extra_param), true); ?>
          </div>
          <div class="bs-callout bs-callout-info" style="margin-top: 40px;">
            <?php require('inc/SF_STORY_BELOW-TITLE_728x90.php'); ?>
          </div>
    </main>

    <aside class="HolyGrail-right hidden-mid tnl-subject-wrap" style="background-color:rgb(230, 230, 230);">
<?php if($is_sponsor_tag): ?>
<?php
        $sp1 = dirname( __FILE__ ) . '/sponsor/tag/' . $sponsor_id . '1.php';
        if(is_file($sp1)){
            require($sp1);
        }
?>
<?php else: ?>
      <div class="tnl-panel">
        <?php require('inc/SF_STORY_SIDEBAR-HEAD_300x250.php'); ?>
      </div>
<?php endif; ?>
      <?php require('inc/follow_us.php'); ?>
<?php if($is_sponsor_tag): ?>
<?php
    $sp2 = dirname( __FILE__ ) . '/sponsor/tag/' . $sponsor_id . '2.php';
    if(is_file($sp2)){
        require($sp2);
    }
?>
<?php endif; ?>
      <div class="tnl-panel">
        <?php require('inc/hot_post.php'); ?>
      </div>
    
    </aside>

  </div>

<?php get_footer(); ?>