<!DOCTYPE html>
<!--
_/\/\/\/\/\/\__/\/\________________________/\/\____/\/\______________________________________________/\/\___________________________________________
_____/\/\______/\/\__________/\/\/\________/\/\/\__/\/\____/\/\/\____/\/\______/\/\____/\/\/\/\______/\/\__________/\/\/\____/\/\/\/\______/\/\/\/\_
_____/\/\______/\/\/\/\____/\/\/\/\/\______/\/\/\/\/\/\__/\/\/\/\/\__/\/\__/\__/\/\__/\/\/\/\________/\/\________/\/\/\/\/\__/\/\__/\/\__/\/\/\/\___
_____/\/\______/\/\__/\/\__/\/\____________/\/\__/\/\/\__/\/\________/\/\/\/\/\/\/\________/\/\______/\/\________/\/\________/\/\__/\/\________/\/\_
_____/\/\______/\/\__/\/\____/\/\/\/\______/\/\____/\/\____/\/\/\/\____/\/\__/\/\____/\/\/\/\________/\/\/\/\/\____/\/\/\/\__/\/\__/\/\__/\/\/\/\___
____________________________________________________________________________________________________________________________________________________
-->
<html <?php language_attributes(); ?>>
  <head>
  <script type="text/javascript">if (self != top) top.location.href = self.location.href;</script>
    <meta charset="utf-8">
    <title><?php wp_title( '|', true, 'right' ); ?></title> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
    <link type="application/rss+xml" rel="alternate" title="訂閱 The News Lens 關鍵評論網" href="http://feeds.feedburner.com/TheNewsLens" />
    <link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<?php require_once( get_template_directory() . '/inc/marketing_header.php' ); ?>
<?php if(is_single()): ?>
<?php
    $post = get_post();
    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');

?>
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "Article",
  "name" : "<?=$post->post_title?>",
  "author" : {
    "@type" : "Person",
    "name" : "<?=get_the_author();?>"
  },
  "datePublished" : "<?=$post->post_date?>",
  "image" : "<?= $image_url[0]?>",
  "articleBody" : "<?=preg_replace('/^\s+|\n|\r|\s+$/m', ' ', summarize_long_text($post->post_content, -1))?>",
  "url" : "<?=get_permalink($post->ID)?>",
  "publisher" : {
    "@type" : "Organization",
    "name" : "The News Lens"
  }
}
</script>
<?php endif; ?>
<?php require_once( get_template_directory() . '/inc/dfp_head.php' ); ?>
<?php require_once( get_template_directory() . '/inc/onead.php' ); ?>
  </head>
  <body class="HolyGrail" style="">
<?php require_once( get_template_directory() . '/inc/alexa_cert.php' ); ?>

<?php require_once( get_template_directory() . '/inc/google_analytics.php' ); ?>

<style type="text/css">
.none {display:none}
</style>
<script type="text/javascript">    
jQuery(document).ready(function(){
    var footerwidth = jQuery("footer").width(); //抓footer元素的總寬       
    if(footerwidth < 1200 ){
      jQuery("#classify").removeClass("none"); //移除一個class 讓廣告顯示
    }
    if(footerwidth > 1200 ){
      jQuery("#classify").addClass("none"); //新增一個class 讓廣告消失
    }
});
</script>

<?php require('inc/social_media_api_header.php'); ?>
    <nav class="main-nav" style="height:50px; background-color:#fff;">
    <div id="classify" style="float:left; padding:15px; line-height: 20px;border-right-style: solid; border-width: 1px; border-color: #E0E0E0;">
      <a href="/category-list/" style="color:black;"><span class="glyphicon glyphicon-th-list" style="margin: 2px; float: left;"></span><span class="hidden-xs">分類</span></a>
    </div>
    <div style="float:left;text-align:center;width:250px;">
    <a href="/">
      <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" 
      class="img-responsive" width="200" height="auto" alt="" 
      style="min-width:160px; max-width: 200px; margin: 0px auto; line-height: 20px;padding: 14px 0 0 0;vertical-align : middle;">
    </a>
    </div>
    <div class="hidden-mid" style="float:left; padding:15px 0 0 30px; line-height: 20px;"><a href="/category-list/" style="color:black;"><span class="glyphicon glyphicon-th-list" style="margin: 2px; float: left;"></span>內容分類</a></div>
    <div class="hidden-mid" style="float:left; padding:15px; line-height: 20px;"><a href="/news-list/" style="color:black;"><span class="glyphicon glyphicon-align-left" style="margin: 2px; float: left;"></span>新聞</a></div>
    <div class="hidden-mid" style="float:left; padding:15px; line-height: 20px;"><a href="/review-list/" style="color:black;"><span class="glyphicon glyphicon-comment" style="margin: 2px; float: left;"></span>評論</a></div>
    <div class="hidden-mid" style="float:left; padding:15px; line-height: 20px;"><a href="/tag/campus-authors/" style="color:black;"><span class="glyphicon glyphicon-book" style="margin: 2px; float: left;"></span>校園</a></div>
    <div class="hidden-mid" style="float:left; padding:15px; line-height: 20px;"><a href="/tag/city-authors/" style="color:black;"><span class="glyphicon glyphicon-globe" style="margin: 2px; float: left;"></span>城市</a></div>
    <div class="hidden-mid" style="float:left; margin:15px; line-height: 20px;"><a href="/topic/videos/" style="color:black;"><span class="glyphicon glyphicon-play-circle" style="margin: 2px; float: left;"></span>影片</a></div>
    <div class="hidden-mid" style="float:left; margin:15px; line-height: 20px;"><a href="http://forum.thenewslens.com/?ref=forumpost" target='_blank' style="color:black;"><span class="glyphicon glyphicon-glass" style="margin: 2px; float: left;"></span>論壇</a></div>
    <div class="hidden-mid" style="float:left; padding:15px; line-height: 20px;">
      <div id="wrap">
        <form action="<?php echo home_url( '/' ) . 'search/'; ?>" autocomplete="on">
          <input id="search" style="" name="query" type="text" placeholder="您在尋找什麼？"><input id="search_submit" value="搜尋" type="submit">
        </form>
      </div>
    </div>     
  <div class="hidden-xxs" style="float:right; margin:8px; line-height: 20px;">
  <div class="btn-group hidden-mid" style="margin-right:4px;">
        <button type="button" id="btn-t" class="btn btn-default active">正體</button>
        <button type="button" id="btn-s" class="btn btn-default">简体</button>
      </div>
      <?php 
                                    $user_loggedin = menu_login_logout();
                                    echo $user_loggedin;
       ?>
    </div>
    <div class="visible-lg" style="width: 150px; float:right; margin:15px; line-height: 20px;"></div>
    

    </nav>
<?php 
if( is_home() ){
  require('inc/hero_content.php');
  require('inc/hot_topic.php');
} else if( is_page() ) {
  require('inc/page_title.php');
}