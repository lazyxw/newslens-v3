<?php
require_once('lib/config.php');
require_once('lib/helper.php');
require_once('lib/get_data.php');

// config of feature story
$fs_config_file = get_template_directory() . '/sponsor/feature_story/config.php';
if(is_file($fs_config_file)){
    require($fs_config_file);
}

// remove admin bar in frontend
add_filter('show_admin_bar', '__return_false');

function tnl_load_css() {
    if (is_admin()) return;

    wp_enqueue_style('bootstrap.min', get_template_directory_uri() . '/dist/css/bootstrap.min.css');
    wp_enqueue_style('tnl', get_template_directory_uri() . '/tnl.css');
    wp_enqueue_style('tnl-add', get_template_directory_uri() . '/tnl-add.css');
    wp_enqueue_style('tnl-topic', get_template_directory_uri() . '/topics.css');
    wp_enqueue_style('google-font', 'http://fonts.googleapis.com/css?family=Radley');
    wp_enqueue_style('iealert', get_template_directory_uri() . '/dist/iealert/style.css');
}
add_action('init', 'tnl_load_css');


function tnl_load_js() {

    if (is_admin()) return;

    wp_enqueue_script('jquery', false, false, 1, false);
    wp_enqueue_script('jquery.timeago', get_template_directory_uri() . '/dist/js/jquery.timeago.js', array( 'jquery' ), 1, true);
    wp_enqueue_script('jquery.timeago.zh-TW', get_template_directory_uri() . '/dist/js/jquery.timeago.zh-TW.js', array( 'jquery', 'jquery.timeago' ), 1, true);

    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/dist/js/bootstrap.min.js', false, 1, true);

    // JS: cookie
    wp_enqueue_script('jquery-cookie', get_template_directory_uri() . '/dist/js/jquery.cookie.js', array( 'jquery' ), 1, false);

    // JS: ie alert
    wp_enqueue_script('iealert', get_template_directory_uri() . '/dist/js/iealert.js', array( 'jquery' ), 1, false);
    
    //JS: top
    wp_enqueue_script('easing', get_template_directory_uri() . '/dist/js/jquery.easing.1.3.js','', 1, true);
    wp_enqueue_script('jstop', get_template_directory_uri() . '/dist/js/jquery.ui.totop.js','', 1, true);

    // js: lang
    wp_enqueue_script('tcsc-converter', get_template_directory_uri() . '/dist/js/langs.js','', 1, false);
    
    // JS: flyout post nav
    // wp_enqueue_script('jquery.flyout-post-nav', get_template_directory_uri() . '/dist/js/jquery.nextPost.js', array( 'jquery' ), 1, true);

    wp_enqueue_script( 'tnl', get_template_directory_uri() . '/dist/js/tnl-pack.js', false, 1, true);
}
add_action('init', 'tnl_load_js');



// add extra contact info
function extra_contact_info($contactmethods) {
    unset($contactmethods['aim']);
    unset($contactmethods['yim']);
    unset($contactmethods['jabber']);
    
    $contactmethods['linkedin'] = 'linkedIn';
    $contactmethods['youtube'] = 'youtube';
    
    return $contactmethods;
}
add_filter('user_contactmethods', 'extra_contact_info');


// remove admin bar in frontend
add_filter('show_admin_bar', '__return_false');

//set the content width
if ( ! isset( $content_width ) ) $content_width = 720;


//the image sizes that we use
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size(66, 66, true );
add_image_size('art-thumb', 66, 66, true);
add_image_size('art-big', 720, 375, true);
add_image_size('art-big-2col', 346, 154, true);
add_image_size('art-gal', 220, 220, true);
add_image_size('side', 300, 110, true);
add_image_size('arhive', 220, 132, true);



// Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'feed_links', 2 ); 


// add rewrite rule for author link - support /author/author_nicename
function wpdocs_author_rewrites( $author_rewrite ) {
    $contributors = array(
        'author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$' => 'index.php?author_name=$matches[1]&feed=$matches[2]',
        'author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$'      => 'index.php?author_name=$matches[1]&feed=$matches[2]',
        'author/([^/]+)/page/?([0-9]{1,})/?$'             => 'index.php?author_name=$matches[1]&paged=$matches[2]',
        'author/([^/]+)/?$'                               => 'index.php?author_name=$matches[1]'
    );
 
    $old = array();
    // This relies on you registering a 'redirect' query var and handler.
    // The handler would check for 'redirect' and apply a wp_redirect call where necessary.
    foreach ( $author_rewrite as $endpoint => $query )
        $old[$endpoint] = $query . '&redirect=1';
 
    return array_merge( $old, $contributors );
}
add_filter( 'author_rewrite_rules', 'wpdocs_author_rewrites' );

// rewrite author url - remove /post
function my_author_url_with_id($link, $author_id, $author_nicename) {
  $link_base = trailingslashit(get_option('home'));
  $link = "author/$author_nicename/";
  return $link_base . $link;
}
add_filter('author_link', 'my_author_url_with_id', 1000, 3);

// Modifies RSS permalinks to use Google Analytics tracking
function trackedrss($guid) {
    if (is_feed()) {
        if(!empty($_GET['from'])){
            $operator = (preg_match("/\?/i", $guid)) ? "&amp;":"?";
            $format = "%s"."utm_source=%s"."&amp;utm_medium=%s"."&amp;utm_campaign=%s";
            $url_pre = $guid . $operator;
            $source = strip_tags($_GET['from']);
            $medium = "feed";
            $campaign = '3rd+party';
            return sprintf($format,$url_pre,$source,$medium,$campaign);
        } else {
            return $guid;
        }
    }
}
add_filter ('the_permalink_rss','trackedrss');

// remove backspace character in content for feed
function clearChr($content) {
    if(is_feed()){
        $content = str_replace(chr(8), '', $content);
    }
    return $content;
}
add_filter('the_content', 'clearChr');

// redirect /post to home
function my_404_override() {
    global $wp_query;

    if ( is_404() ) {
        $redirect = parse_url($_SERVER['REQUEST_URI']);

        if ( '/post/' == $redirect['path'] or '/post' == $redirect['path']){
            status_header( 200 );
            $wp_query->is_404=false;
            wp_redirect( home_url(), 301 ); 
            exit;
        }
    }
}
add_filter('template_redirect', 'my_404_override' );

// use summary for kollect's feed
function summary_feed( $content ){
    if( is_feed() and !empty($_GET['from']) and $_GET['from'] == 'kollect' ) {
        $content = summarize_long_text($content, 200) . '...';
    }

  return $content;
}
add_filter('the_content', 'summary_feed' );

// add feed for opinion only, news only, video only
function feedAboutNews($query) {
    if( is_feed() ) {
        if( !empty($_GET['ro']) ) {
            $query->set('cat','-2758,-12049');
        } else if( !empty($_GET['no']) ) {
            $query->set('cat','12049');
        } else if( !empty($_GET['vo']) ) {
            $query->set('cat','2758');
        }
    }
    // return $query;
}
add_filter('pre_get_posts','feedAboutNews');

// feed for 4free wifi
function feedFor4free($content) {
    if( is_feed() and !empty($_GET['from']) and $_GET['from'] == '4free' ) {
        // Global $post variable
        global $post;
        // Check if the post has a featured image
        if (has_post_thumbnail($post->ID)) {
            $content = '<div class="content_summary">' . summarize_long_text($content, 160) . '...</div>';
            $content = '<div class="featured_image_post_rss">' . get_the_post_thumbnail($post->ID, 'full') . '</div>' . $content;
        }
    }
    
    return $content;
}

//Add the filter for RSS feeds Excerpt
add_filter('the_excerpt_rss', 'feedFor4free');
//Add the filter for RSS feed content
add_filter('the_content_feed', 'feedFor4free');

// filter meta desc for yoast seo
function filter_meta_desc( $str ) {
  return preg_replace('/Photo(.*?)2.0/i', '', $str);
}
add_filter( 'wpseo_metadesc', 'filter_meta_desc', 10, 1 );

// Stop WordPress Editor from Removing iFrame and Embed Code
function mytheme_tinymce_config( $init ) {
    $valid_iframe = 'iframe[id|class|title|style|align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width]';
    if ( isset( $init['extended_valid_elements'] ) ) {
        $init['extended_valid_elements'] .= ',' . $valid_iframe;
    } else {
        $init['extended_valid_elements'] = $valid_iframe;
    }
    return $init;
}
add_filter('tiny_mce_before_init', 'mytheme_tinymce_config');

// add menu to manage hero content
function hero_content_options(){
    if( isset($_GET['settings-updated']) and $_GET['settings-updated'] == 'true' ) {
?>
    <div id="message" class="updated">
        <p><strong><?php _e('Settings saved.') ?></strong></p>
    </div>
<?php } ?>
    <div class="wrap">
        <h2>首頁設定</h2>
        <form method="post" action="options.php">
        <fieldset style="border:1px solid #ccc;padding:5px;">
            <legend>Hero Content</legend>
            <?php wp_nonce_field('update-options') ?>
            <p><strong>Post ID:</strong> (請輸入要放在首頁上的 post id)<br />
                <input type="text" name="hero_content_post_id" size="50" value="<?php echo get_option('hero_content_post_id'); ?>" />
            </p>
            <p><strong>熱門議題:</strong> (請使用半型逗號區隔，中間不留空白，如: 服貿,太陽花學運)<br />
                <input type="text" name="hero_content_tag" size="50" value="<?php echo get_option('hero_content_tag'); ?>" />
            </p>
            <p><input type="submit" name="Submit" value="Save" /></p>
            <input type="hidden" name="action" value="update" />
            <input type="hidden" name="page_options" value="hero_content_post_id,hero_content_tag" />
        </fieldset>
        </form>
        <br>
        <form method="post" action="options.php">
        <fieldset style="border:1px solid #ccc;padding:5px;">
            <legend>Marketing Block</legend>
            <?php wp_nonce_field('update-options') ?>
            <p><strong>Subject:</strong><br />
                <input type="text" name="marketing_block_subject" size="50" value="<?php echo get_option('marketing_block_subject'); ?>" />
            </p>
            <p><strong>Link:</strong> (ex: http://www.thenewslens.com/)<br />
                <input type="text" name="marketing_block_link" size="50" value="<?php echo get_option('marketing_block_link'); ?>" />
            </p>
            <p><strong>Image:</strong> (ex: http://www.thenewslens.com/wp-content/uploads/2014/05/event_logo.gif)<br />
                <input type="text" name="marketing_block_image" size="50" value="<?php echo get_option('marketing_block_image'); ?>" />
            </p>
            <p><input type="submit" name="Submit" value="Save" /></p>
            <input type="hidden" name="action" value="update" />
            <input type="hidden" name="page_options" value="marketing_block_subject,marketing_block_link,marketing_block_image" />
        </fieldset>
        </form>
    </div>
<?php
}

function add_hero_content_options(){
    add_posts_page('首頁設定', '首頁設定', 'edit_others_posts', 'hero_content','hero_content_options');
}
add_action('admin_menu', 'add_hero_content_options');

// custom template for photo gallery
function get_gallery_template($single_template) {
    global $post;
 
    if ( in_category( '圖片' )) {
          $single_template = dirname( __FILE__ ) . '/photo_gallery.php';
    }
    return $single_template;
}
add_filter( "single_template", "get_gallery_template" ) ;

// custom template for sponsor content
function get_sponsor_template($single_template) {
    global $post;
    $is_sponsor_author = get_user_meta($post->post_author, 'sponsor_author', true);

    if ( '1' === $is_sponsor_author ) {
          $single_template = dirname( __FILE__ ) . '/sponsor_content.php';
    }
    return $single_template;
}
add_filter( "single_template", "get_sponsor_template" ) ;

// custom og image for sponsor tag
function custom_og_image($val) {
    
    if( is_tag() and get_query_var('tag') == 'pm2-5' ){ // sponsor tag
        $val = 'http://www.thenewslens.com/wp-content/themes/newslens_v3/sponsor/tag/pm2-5.jpg';
    } else if( is_single() ) { // general post
        global $post;
        $val = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'thumbnail') );
    }

    return $val;
}
add_filter('wpseo_opengraph_image', 'custom_og_image');

// take off ..... from excerpt
function custom_excerpt_more( $more ) {
	return "";
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );

/**
 * Register custom RSS template.
 */

/**
 * Custom RSS template callback.
 */
function msn_custom_rss_render() {
    get_template_part( 'feed', 'msn' );
}
function cht_custom_rss_render() {
    get_template_part( 'feed', 'cht' );
}
function htc_custom_rss_render() {
    get_template_part( 'feed', 'htc' );
}
function firefox_custom_rss_render() {
    get_template_part( 'feed', 'firefox' );
}
function newsletter_custom_rss_render() {
    get_template_part( 'feed', 'newsletter' );
}

function rss_custom_template() {
    // feed for msn
    add_feed( 'msn', 'msn_custom_rss_render' );
    // feed for cht
    add_feed( 'cht', 'cht_custom_rss_render' );
    // feed for htc
    add_feed( 'htc', 'htc_custom_rss_render' );
    // feed for firefox
    add_feed( 'firefox', 'firefox_custom_rss_render' );
    // feed for newsletter
    add_feed( 'newsletter', 'newsletter_custom_rss_render' );
}

add_action( 'login_head', 'hide_login_nav' );

function hide_login_nav()
{
    ?><style>#nav{display:none}</style><?php
}

// custom rss feed
add_action( 'init', 'rss_custom_template' );
